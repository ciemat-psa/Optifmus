#include "experiment.h"
#include "ui_experiment.h"

#include <QFileDialog>
#include <QFontMetrics>
#include <QMessageBox>

#include "ui_conf.h"
#include "common.h"
#include "commonapp.h"

const QString cbFree  = "None";
const QString cbFixed = "Fixed value";
const QString cbFile  = "From file";

const QString cbTimeDist = "Linear distribution";
const QString cbTimeFile = "From file";

const int COL_NAME    = 0;
const int COL_DESC    = 1;
const int COL_UNIT    = 2;
const int COL_TYPE    = 3;
const int COL_VALUE   = 4;
const int COL_BUT     = 5;
const int COL_LAST    = 6;

const int WIDTH_NAME  = 200;
const int WIDTH_DESC  = 220;
const int WIDTH_UNIT  = 75;
const int WIDTH_TYPE  = 100;
const int WIDTH_VALUE = 150;
const int WIDTH_BUT   = 30;
const int WIDTH_LAST  = 10;

const QString PRO_POS   = "PRO_POS";
const QString PRO_FILE  = "PRO_FILE";
const QString PRO_VALUE = "PRO_VALUE";
const QString PRO_ITEM  = "PRO_ITEM";
const QString PRO_TB    = "PRO_TB";
const QString PRO_LE    = "PRO_LE";

const unsigned DATA_TYPE = 1;

experiment::experiment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::experiment)
{
    ui->setupUi(this);
    readGeometry(this,expWin);

    fileStatus  = FILE_NO_EXIST;
    fileChanged = false;
    exp.fileName.clear();

    // Texts and icons
    setWindowTitle(tExpConf);
    ui->lTitle->setText(tExperiment);
    ui->gbFileTime->setTitle(tFileTimeVar);
    ui->lbiFilename->setText(tFilename);
    ui->lbiFilter->setText(tFilter);
    ui->lbiTime->setText(tTime);
    ui->lbiVariable->setText(tVariable);
    ui->cbCaseSentitive->setText(tCaseSensitive);
    ui->lbiInitial->setText(tInitialS);
    ui->lbiFinal->setText(tFinalS);
    ui->lbiIntervals->setText(tNIntervals);
    ui->lbiFileType->setText(tFileType);
    ui->lbiVars->setText(tNumVar);
    ui->lbiMeasures->setText(tNumMea);
    ui->lbiFilename->setText(tFile);
    ui->lbiFileStatus->setText(tFileStatus);

    ui->lbFileType->setStyleSheet(clBlue);
    ui->lbVars->setStyleSheet(clBlue);
    ui->lbMeasures->setStyleSheet(clBlue);
    ui->lbFileStatus->setStyleSheet(clGreen);

    ui->lbFileType->setText(sEmpty);
    ui->lbVars->setText(sEmpty);
    ui->lbMeasures->setText(sEmpty);
    ui->lbFileStatus->setText(sEmpty);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);
    const QIcon icRefresh = QIcon(iRefresh);
    ui->tbRefresh->setIcon(icRefresh);
    const QIcon icField = QIcon(iField);
    ui->tbTimeFile->setIcon(icField);
    const QIcon icDocImp = QIcon(iDocImp);
    ui->tbDocImport->setIcon(icDocImp);
    const QIcon icExp = QIcon(iExperiment);
    ui->tbExpImport->setIcon(icExp);
    const QIcon icOpen = QIcon(iLoadProject);
    ui->bOpen->setIcon(icOpen);
    const QIcon icPlot = QIcon(iPlot);
    ui->bPlot->setIcon(icPlot);

    ui->tbDocImport->setToolTip(sbDocImport);
    ui->tbExpImport->setToolTip(sbExpImport);


    // Configuration
    ui->cbTimeType->insertItem(ttTimeDist,cbTimeDist);
    ui->cbTimeType->insertItem(ttTimeFile,cbTimeFile);
    ui->cbTimeType->setCurrentText(cbTimeDist);
    ui->leTimeFile->setEnabled(false);
    ui->leInitial->setValidator(new QDoubleValidator(0,MAX_DOUBLE, NUM_DEC, this));
    ui->leFinal->setValidator(new QDoubleValidator(0,MAX_DOUBLE, NUM_DEC, this));

    // Properties
    ui->tbTimeFile->setProperty(PRO_LE.toLatin1().data(),qVariantFromValue((void *)ui->leTimeFile));

    checkEnabled();

    // Tree headers
    QStringList headerLabels;
    headerLabels.push_back(tInput);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tType);
    headerLabels.push_back(tValue);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_TYPE,WIDTH_TYPE);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_BUT,WIDTH_BUT);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);

    readState(ui->tree->header(),treeExp);
}

experiment::~experiment()
{
    writeGeometry(this,expWin);
    writeState(ui->tree->header(),treeExp);
    delete ui;
}

void experiment::checkEnabled()
{
    // Tool button - time variable
    ui->tbTimeFile->setEnabled(fileStatus == FILE_OK);

    // Tool buttons - tree
    QTreeWidgetItemIterator it(ui->tree);
    while (*it)
    {
        QWidget *qw = ui->tree->itemWidget(*it,COL_BUT);
        if (qw!=NULL && qw->inherits("QToolButton"))
            ((QToolButton *)qw)->setEnabled(fileStatus == FILE_OK);
        ++it;
    }

    // Refresh button
    ui->tbRefresh->setEnabled(fileStatus == FILE_OK);
    ui->bPlot->setEnabled(fileStatus == FILE_OK);
    ui->tbDocImport->setEnabled(fileStatus == FILE_OK && ui->cbTimeType->currentIndex() == ttTimeFile && !ui->leTimeFile->text().isEmpty());
    ui->tbExpImport->setEnabled(mo.expData && mo.expStart>=0 && mo.expStop>=0);
    ui->sbReducSam->setEnabled(ui->cbTimeType->currentIndex() == ttTimeFile);

    // Accept button
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(ui->leInitial->text().toDouble()<ui->leFinal->text().toDouble());
}

bool experiment::foundVariable(QString name)
{
    for(int i=0;i<vars.count();i++)
    {
        if (vars[i].name == name) return true;
    }
    return false;
}

void experiment::clearFields()
{
    // Tool button - time variable
    if (!foundVariable(ui->leTimeFile->text()))
        ui->leTimeFile->clear();

    // Tool buttons - tree
    QTreeWidgetItemIterator it(ui->tree);
    while (*it)
    {
        QWidget *qw = ui->tree->itemWidget(*it,COL_VALUE);
        QWidget *qc = ui->tree->itemWidget(*it,COL_TYPE);
        if (qw!=NULL && qc->inherits("QComboBox") && (((QComboBox *)qc)->currentText() == cbFile) && qw->inherits("QLineEdit"))
        {
            if ((fileStatus != FILE_OK) || (!foundVariable(((QLineEdit *)qw)->text())))
                ((QLineEdit *)qw)->clear();
        }
        ++it;
    }
}

void experiment::on_cbTimeType_currentIndexChanged(const QString &arg1)
{
    ui->swTime->setCurrentIndex(arg1 == cbTimeFile);
    checkEnabled();
}

void experiment::on_bOpen_clicked()
{
    QString file = fileAbsoluteFromProject(prj,exp.fileName);

    file = QFileDialog::getOpenFileName(this, mSelectInpFile, file, TCFilter + ";;" + TRJFilter + ";;" + CSVFilter);    
    if (!file.isEmpty())
    {
            setFile(file);
            clearFields();
            checkEnabled();
    }
}

void experiment::setFile(QString file)
{
    QString  error;
    int      num;

    if (!file.isEmpty())
    {
        // Check file format
        this->setUpdatesEnabled(false);
        fileStatus  = checkInputFile(file,exp.fileFormat,vars,first,last,num,error);
        this->setUpdatesEnabled(true);
        fileChanged = true;
        if (fileStatus == FILE_OK)
        {
            // Set variables
            exp.fileName = fileRelativeToProject(prj,file);

            // Set values in GUI
            QFontMetrics fm(ui->eFile->font());
            ui->eFile->setText(fm.elidedText(file,Qt::ElideMiddle,ui->eFile->width()-WIDTH_EPS));
            ui->eFile->setToolTip(file);
            ui->lbFileStatus->setStyleSheet(clGreen);
            ui->lbFileStatus->setText(tOk);
            ui->lbFileType->setText(exp.fileFormat == ffCSV ? tCSV : tTrajectory);
            ui->lbVars->setText(QString::number(vars.size(),NUM_FORMAT,NUM_DEC));
            ui->lbMeasures->setText(QString::number(num,NUM_FORMAT,NUM_DEC));
        }
        else
        {
            ui->lbFileType->clear();
            ui->lbVars->clear();
            ui->lbMeasures->clear();
            ui->lbFileStatus->setStyleSheet(clRed);
            switch(fileStatus){
                case FILE_NO_EXIST:
                    ui->lbFileStatus->setText(mFileNoExist);
                    //QMessageBox::critical(this,mInputFile,mFileNoExist);
                    break;
                case FILE_UNSUPPORTED:
                    ui->lbFileStatus->setText(mFileUnsupported);
                    //QMessageBox::critical(this,mInputFile,mFileUnsupported);
                    break;
                case FILE_WRONG_FORMAT:
                {
                    if (exp.fileFormat == ffCSV)
                        ui->lbFileStatus->setText(mFileCSVWrongFromat);
                        //QMessageBox::critical(this,mInputFile,mFileCSVWrongFromat);
                    else
                        ui->lbFileStatus->setText(error);
                        //QMessageBox::critical(this,mInputFile,error);
                    break;
                }
            }
        }
    }
}

void experiment::on_eFile_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    checkEnabled();
}

void experiment::on_toolButton_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void experiment::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void setItemTreeExp(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int pos, QList<QWidget *> &lw)
{
    Q_UNUSED(v);
    Q_UNUSED(vs);
    Q_UNUSED(lw);
    // WARNING: only real inputs supported right now.

    opt_model mo  = prj.getModel();
    opt_exp   exp = prj.getExp();

    item->setText(COL_DESC,mo.inputs[pos].desc);
    item->setText(COL_UNIT,formatedUnit(mo,mo.inputs,pos));
    item->setData(DATA_TYPE,Qt::UserRole,mo.inputs[pos].type);
    switch (mo.inputs[pos].type)
    {
        case FMI_REAL:
        {
            QComboBox *cb = new QComboBox(NULL);
            cb->addItem(cbFree);
            cb->addItem(cbFixed);
            cb->addItem(cbFile);
            cb->connect(cb,SIGNAL(currentIndexChanged(const QString &)),tree->parent(),SLOT(on_cb_currentIndexChanged(const QString &)));
            cb->setProperty(PRO_POS.toLatin1().data(),pos);
            cb->setProperty(PRO_ITEM.toLatin1().data(),qVariantFromValue((void *) item));
            cb->setCurrentText(cbFree);
            cb->setMinimumHeight(WIDTH_BUT);
            cb->setMaximumHeight(WIDTH_BUT);
            item->setIcon(COL_NAME,QIcon(iReal));
            tree->setItemWidget(item,COL_TYPE,cb);

            // Changed input value
            int index = exp.model_inputs.indexOf(mo.inputs[pos].name);
            if (index>=0)
            {
                cb->setCurrentIndex(exp.type_inputs[index]);
                QWidget *le = tree->itemWidget(item,COL_VALUE);
                if (le!=NULL && le->inherits("QLineEdit"))
                    ((QLineEdit *)le)->setText(exp.type_inputs[index] == ttFile ? exp.file_inputs[index] : exp.fixed_inputs[index]);
            }
            break;
        }
        default:
            break;
    }
}

void experiment::setModelExp(opt_project *iprj)
{
    // Set experiment and model
    prj = iprj;
    exp = prj->getExp();
    mo  = prj->getModel();

    // Set file
    setFile(fileAbsoluteFromProject(prj,exp.fileName));

    // Set sample reduction
    ui->sbReducSam->setValue(exp.ReducSam);

    // Set time info
    ui->leInitial->setText(QString::number(exp.startTime,NUM_FORMAT,NUM_DEC));
    ui->leFinal->setText(QString::number(exp.stopTime,NUM_FORMAT,NUM_DEC));
    ui->sbStep->setValue(exp.nInInt);
    ui->leTimeFile->setText(exp.timeVar);
    ui->cbTimeType->setCurrentIndex(exp.timeInType);

    // Populate tree
    populateTreeGeneric(ui->tree,mo.inputs,*prj,COL_NAME,sEmpty,setItemTreeExp,lw);

    // Chec enabled buttons
    checkEnabled();
}

void experiment::on_cb_currentIndexChanged(const QString &)
{
    QComboBox       *cb = (QComboBox *)sender();
    QTreeWidgetItem *item;
    QString         opt;
    int             pos;


    pos  = cb->property(PRO_POS.toLatin1().data()).toInt();
    item = (QTreeWidgetItem *)cb->property(PRO_ITEM.toLatin1().data()).value<void *>();
    opt  = cb->currentText();
    deleteWidgets(item);
    if (opt == cbFile)
    {
        QLineEdit   *le = new QLineEdit(this);
        QToolButton *tb = new QToolButton(this);

        le->setProperty(PRO_POS.toLatin1().data(),pos);
        tb->setIcon(QIcon(iField));
        tb->setToolButtonStyle(Qt::ToolButtonIconOnly);
        tb->setMinimumHeight(WIDTH_BUT);
        tb->setMaximumHeight(WIDTH_BUT);
        tb->setMaximumWidth(WIDTH_BUT);
        tb->setProperty(PRO_LE.toLatin1().data(),qVariantFromValue((void *)le));
        tb->setEnabled(fileStatus == FILE_OK);
        connect(tb,SIGNAL(clicked()),this,SLOT(on_tb_clicked()));
        le->setMinimumHeight(WIDTH_BUT);
        le->setMaximumHeight(WIDTH_BUT);
        le->setEnabled(false);
        ui->tree->setItemWidget(item,COL_VALUE,le);
        ui->tree->setItemWidget(item,COL_BUT,tb);
    }
    else if (opt == cbFixed)
    {
        QLineEdit        *ler = new QLineEdit(this);
        QDoubleValidator *dv  = new QDoubleValidator(this);

        dv->setDecimals(NUM_DEC);
        ler->setProperty(PRO_POS.toLatin1().data(),pos);
        ler->setValidator(dv);
        ler->setMinimumHeight(WIDTH_BUT);
        ler->setMaximumHeight(WIDTH_BUT);
        ui->tree->setItemWidget(item,COL_VALUE,ler);
    }
    else // cbFree
    {

    }
}

void experiment::deleteWidgets(QTreeWidgetItem *item)
{
    ui->tree->removeItemWidget(item,COL_VALUE);
    ui->tree->removeItemWidget(item,COL_BUT);
}

void experiment::on_tb_clicked()
{
    QToolButton *tb = (QToolButton *)sender();
    QLineEdit   *le = (QLineEdit *)tb->property(PRO_LE.toLatin1().data()).value<void *>();

    // Busy cursor
    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Populate tree
    if (fileChanged)
    {
        ft.setInfo(fileAbsoluteFromProject(prj,exp.fileName),exp.fileFormat == ffCSV ? tCSV : tTrajectory);
        ft.populateTree(vars,le->text());
        fileChanged = false;
    }
    else
    {
        ft.clearFilter();
        ft.setSelectedItem(le->text());
    }

    // Restore cursor
    QApplication::restoreOverrideCursor();

    if (ft.exec())
        le->setText(ft.selectedItem());
}

void experiment::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void experiment::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void experiment::on_tbTimeFile_clicked()
{
    // Busy cursor
    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Populate tree
    if (fileChanged)
    {
        this->setUpdatesEnabled(false);
        ft.setInfo(fileAbsoluteFromProject(prj,exp.fileName),exp.fileFormat == ffCSV ? tCSV : tTrajectory);
        ft.populateTree(vars,ui->leTimeFile->text());
        this->setUpdatesEnabled(true);
        fileChanged = false;
    }
    else
    {
        ft.clearFilter();
        ft.setSelectedItem(ui->leTimeFile->text());
    }
    // Restore cursor
    QApplication::restoreOverrideCursor();

    if (ft.exec())
        ui->leTimeFile->setText(ft.selectedItem());

    checkEnabled();
}

opt_exp experiment::getExp()
{
    return exp;
}

void experiment::on_buttonBox_accepted()
{
    // Store information in experiment (file already store)

    // Time information
    exp.startTime  = ui->leInitial->text().toDouble();
    exp.stopTime   = ui->leFinal->text().toDouble();
    exp.nInInt     = ui->sbStep->value();
    exp.timeInType = ui->cbTimeType->currentIndex();
    exp.timeVar    = ui->leTimeFile->text();
    exp.ReducSam   = ui->sbReducSam->value();

    // New inputs information
    exp.file_inputs.clear();
    exp.type_inputs.clear();
    exp.fmiType_inputs.clear();
    exp.model_inputs.clear();
    exp.fixed_inputs.clear();

    // Iterate over tree
    QTreeWidgetItemIterator it(ui->tree);
    while (*it)
    {
        QWidget *w1 = ui->tree->itemWidget(*it,COL_TYPE);
        if (w1!=NULL && w1->inherits("QComboBox"))
        {
            unsigned type = ((QComboBox *)w1)->currentIndex();
            int      pos  = ((QComboBox *)w1)->property(PRO_POS.toLatin1().data()).toInt();
            if (type == ttFile || type == ttFixed)
            {
                QWidget *w2 = ui->tree->itemWidget(*it,COL_VALUE);
                if (w2!=NULL && w2->inherits("QLineEdit"))
                {
                    QString sValue = ((QLineEdit *)w2)->text();

                    exp.model_inputs.append(mo.inputs[pos].name);
                    exp.type_inputs.append(type);
                    exp.fmiType_inputs.append((*it)->data(DATA_TYPE,Qt::UserRole).toUInt());
                    exp.file_inputs.append(type  == ttFile  ? sValue : sEmpty);
                    exp.fixed_inputs.append(type == ttFixed ? sValue : sEmpty);
                    //qDebug() << exp.model_inputs.last() << " " << exp.type_inputs.last() << " " << exp.file_inputs.last() << " " << exp.fixed_inputs.last();
                }
            }
        }
        ++it;
    }
}

void experiment::on_tbExpImport_clicked()
{
    ui->leInitial->setText(QString::number(mo.expStart,NUM_FORMAT,NUM_DEC));
    ui->leFinal->setText(QString::number(mo.expStop,NUM_FORMAT,NUM_DEC));
}

void experiment::on_tbDocImport_clicked()
{
    int index = searchVar(vars,ui->leTimeFile->text());
    if (index>=0)
    {
        if (index<first.size()) ui->leInitial->setText(QString::number(first[index],NUM_FORMAT,NUM_DEC));
        if (index<last.size())  ui->leFinal->setText(QString::number(last[index],NUM_FORMAT,NUM_DEC));
    }
}
void experiment::on_tbRefresh_clicked()
{
    setFile(fileAbsoluteFromProject(prj,exp.fileName));
    clearFields();
    checkEnabled();
}

void experiment::on_bPlot_clicked()
{
    plotgraphs *plot = new plotgraphs();

    plot->loadFile(fileAbsoluteFromProject(prj,exp.fileName));
    plot->setAttribute(Qt::WA_DeleteOnClose);
    plot->setWindowModality(Qt::ApplicationModal);
    plot->show();
}

void experiment::on_leInitial_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    checkEnabled();
}

void experiment::on_leFinal_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    checkEnabled();
}
