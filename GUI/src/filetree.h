#ifndef FILETREE_H
#define FILETREE_H

#include <QDialog>
#include <QKeyEvent>
#include <QTreeWidgetItem>

#include "opt_project.h"

namespace Ui {
class fileTree;
}

class fileTree : public QDialog
{
    Q_OBJECT

public:
    explicit fileTree(QWidget *parent = 0);
    ~fileTree();
    void setInfo(QString file, QString type);
    void populateTree(QList<variable> vars, QString var_selected);
    void checkEnabled();
    QString selectedItem();
    void setValue(QString itemName);
    bool setSelectedItem(QString var_selected);
    void clearFilter();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void keyPressEvent(QKeyEvent *e);

    void on_lineFilter_returnPressed();

    void on_tbClear_clicked();

    void on_tree_clicked(const QModelIndex &index);

    void on_tree_doubleClicked(const QModelIndex &index);

private:
    Ui::fileTree *ui;
    QList<QWidget *> lw;
};

#endif // FILETREE_H
