#ifndef FMUINFO_H
#define FMUINFO_H

#include <QDialog>
#include <QKeyEvent>

#include "opt_project.h"

namespace Ui {
class FMUinfo;
}

class FMUinfo : public QDialog
{
    Q_OBJECT

public:
    explicit FMUinfo(QWidget *parent = 0);
    ~FMUinfo();
    void populateTree(opt_project prj);
    void saveGeoInfo();

private slots:
    void on_lineFilter_returnPressed();
    void on_tbClear_clicked();
    void keyPressEvent(QKeyEvent *e);

    void on_buttonBox_accepted();

private:
    Ui::FMUinfo *ui;
};

#endif // FMUINFO_H
