#include <QApplication>

#include "qfmioptmain.h"
#include "confapp.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // -------------------- //
    // Locale settings      //
    // -------------------- //
    setlocale(LC_NUMERIC,"C");
    // -------------------- //

    // Define app name and version
    QApplication::setApplicationName(appName);
    QApplication::setApplicationVersion(appVer);

    // Parser and application description
    QCommandLineParser parser;
    parser.setApplicationDescription("OptFMI - Optimization for FMI compliant models");

    // Add help and version options
    parser.addHelpOption();
    parser.addVersionOption();

    // Open project option
    QCommandLineOption cloOpenProject(QStringList() << "p" << "project",
                                      QApplication::translate("main", "Open the project given by <filename>."),
                                      QApplication::translate("main", "filename"));
    parser.addOption(cloOpenProject);

    // Process the actual command line arguments given by the user
    parser.process(a);

    QFMIOptMain w;

    // Process open project option
    QString prjFile = parser.value(cloOpenProject);
    if (!prjFile.isEmpty()) w.load_project(prjFile);

    // Show main window
    w.show();

    return a.exec();
}
