#include "modelstructure.h"
#include "ui_modelstructure.h"

#include "commonapp.h"
#include "ui_conf.h"
#include "opt_project.h"

const int COL_NAME    = 0;
const int COL_CAU     = 1;
const int COL_VAR     = 2;
const int COL_VALUE   = 3;
const int COL_DESC    = 4;
const int COL_UNIT    = 5;
const int COL_MIN     = 6;
const int COL_MAX     = 7;

const int WIDTH_NAME  = 100;
const int WIDTH_CAU   = 35;
const int WIDTH_VAR   = 35;
const int WIDTH_VALUE = 75;
const int WIDTH_DESC  = 250;
const int WIDTH_UNIT  = 60;
const int WIDTH_MIN   = 75;
const int WIDTH_MAX   = 75;

modelStructure::modelStructure(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::modelStructure)
{
    ui->setupUi(this);
    readGeometry(this,strucWin);

    setWindowTitle(tModelStruc);
    ui->lTitle->setText(tModelStruc);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_CAU,WIDTH_CAU);
    ui->tree->setColumnWidth(COL_VAR,WIDTH_VAR);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_MIN,WIDTH_MIN);
    ui->tree->setColumnWidth(COL_MAX,WIDTH_MAX);

    QStringList headerLabels;

    headerLabels.push_back(tName);
    headerLabels.push_back(tCau);
    headerLabels.push_back(tVar);
    headerLabels.push_back(tValue);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tMin);
    headerLabels.push_back(tMax);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    ui->tree->setIconSize(QSize(ST_ICON_SIZE,ST_ICON_SIZE));
    readState(ui->tree->header(),treeStruc);
}

void modelStructure::saveGeoInfo()
{
    writeGeometry(this,strucWin);
    writeState(ui->tree->header(),treeStruc);
}

modelStructure::~modelStructure()
{
    delete ui;
}

void modelStructure::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void modelStructure::on_buttonBox_accepted()
{
    accept();
}

void modelStructure::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void modelStructure::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void setItemTreeMS(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(tree);
    Q_UNUSED(v);
    Q_UNUSED(vs);
    Q_UNUSED(lw);

    opt_model m = prj.getModel();

    // Data type
    if      (m.structure[k].type == FMI_REAL)    {item->setIcon(COL_NAME,QIcon(iReal));       item->setToolTip(COL_NAME,tReal); }
    else if (m.structure[k].type == FMI_INTEGER) {item->setIcon(COL_NAME,QIcon(iInteger));    item->setToolTip(COL_NAME,tInteger); }
    else if (m.structure[k].type == FMI_BOOL)    {item->setIcon(COL_NAME,QIcon(iBool));       item->setToolTip(COL_NAME,tBool); }
    else if (m.structure[k].type == FMI_STRING)  {item->setIcon(COL_NAME,QIcon(iString));     item->setToolTip(COL_NAME,tString); }
    else                                         {item->setIcon(COL_NAME,QIcon(iUndefined));  item->setToolTip(COL_NAME,tUndefined); }

    // Causality
    if      (m.structure[k].cau == FMU_PARAMETER)   {item->setIcon(COL_CAU,QIcon(iParameter));   item->setToolTip(COL_CAU,tParam); }
    else if (m.structure[k].cau == FMU_CALCULATED)  {item->setIcon(COL_CAU,QIcon(iCalculated));  item->setToolTip(COL_CAU,tCalculated); }
    else if (m.structure[k].cau == FMU_INPUT)       {item->setIcon(COL_CAU,QIcon(iInput));       item->setToolTip(COL_CAU,tInput); }
    else if (m.structure[k].cau == FMU_OUTPUT)      {item->setIcon(COL_CAU,QIcon(iOutput));      item->setToolTip(COL_CAU,tOutput); }
    else if (m.structure[k].cau == FMU_LOCAL)       {item->setIcon(COL_CAU,QIcon(iLocal));       item->setToolTip(COL_CAU,tLocal); }
    else if (m.structure[k].cau == FMU_INDEPENDENT) {item->setIcon(COL_CAU,QIcon(iIndependent)); item->setToolTip(COL_CAU,tIndependent); }

    // Variabity
    if      (m.structure[k].var == FMU_CONSTANT)   {item->setIcon(COL_VAR,QIcon(iConstant));   item->setToolTip(COL_VAR,tConstant); }
    else if (m.structure[k].var == FMU_FIXED)      {item->setIcon(COL_VAR,QIcon(iFixed));      item->setToolTip(COL_VAR,tFixed); }
    else if (m.structure[k].var == FMU_TUNABLE)    {item->setIcon(COL_VAR,QIcon(iTunable));    item->setToolTip(COL_VAR,tTunable); }
    else if (m.structure[k].var == FMU_DISCRETE)   {item->setIcon(COL_VAR,QIcon(iDiscrete));   item->setToolTip(COL_VAR,tDiscrete); }
    else if (m.structure[k].var == FMU_CONTINUOUS) {item->setIcon(COL_VAR,QIcon(iContinuous)); item->setToolTip(COL_VAR,tContinuous); }


    item->setText(COL_DESC,m.structure[k].desc);
    item->setText(COL_UNIT,formatedUnit(m,m.structure,k));
    item->setText(COL_VALUE,m.structure[k].value);
    item->setText(COL_MIN,m.structure[k].min);
    item->setText(COL_MAX,m.structure[k].max);
}

void modelStructure::populateTree(opt_project prj)
{
    populateTreeGeneric(ui->tree,prj.getModel().structure,prj,COL_NAME,sEmpty,setItemTreeMS,lw);
}
