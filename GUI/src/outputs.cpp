#include "outputs.h"
#include "ui_outputs.h"

#include "commonapp.h"
#include "ui_conf.h"

const int COL_NAME    = 0;
const int COL_DESC    = 1;
const int COL_UNIT    = 2;
const int COL_LAST    = 3;

const int WIDTH_NAME  = 200;
const int WIDTH_DESC  = 220;
const int WIDTH_UNIT  = 75;
const int WIDTH_LAST  = 10;

outputs::outputs(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::outputs)
{
    ui->setupUi(this);
    readGeometry(this,outWin);

    // Texts and icons
    setWindowTitle(tOutputs);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    // Tree headers
    QStringList headerLabels;
    headerLabels.push_back(tOutput);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);
    ui->tree->setSortingEnabled(true);

    // Configuration
    ui->leTime->setValidator(new QDoubleValidator(0,MAX_DOUBLE, NUM_DEC, this));

    readState(ui->tree->header(),treeOut);
}

outputs::~outputs()
{
    writeGeometry(this,outWin);
    writeState(ui->tree->header(),treeOut);
    delete ui;
}

void outputs::on_lineFilter_2_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter_2->text().trimmed(),ui->cbCaseSentitive_2->checkState() == Qt::Checked);
}

void outputs::on_tbClear_2_clicked()
{
    ui->lineFilter_2->clear();
    on_lineFilter_2_returnPressed();
}

void outputs::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void setItemTreeOut(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int pos)
{
    Q_UNUSED(tree);
    Q_UNUSED(v);
    Q_UNUSED(vs);

    opt_model mo = prj.getModel();

    switch (mo.outputs[pos].type)
    {
        case FMI_REAL:    item->setIcon(COL_NAME,QIcon(iReal));    break;
        case FMI_INTEGER: item->setIcon(COL_NAME,QIcon(iInteger)); break;
        case FMI_BOOL:    item->setIcon(COL_NAME,QIcon(iBool));    break;
        case FMI_STRING:  item->setIcon(COL_NAME,QIcon(iString));  break;
    }

    item->setText(COL_DESC,mo.outputs[pos].desc);
    item->setText(COL_UNIT,formatedUnit(mo,mo.outputs,pos));
}

void outputs::setModelExp(opt_project prj)
{
    // Set experiment
    exp = prj.getExp();
    mo  = prj.getModel();

    // Set sampling info
    ui->rbIntervals->setChecked(exp.timeOutType == ttTimeDist);
    ui->rbTime->setChecked(exp.timeOutType == ttTimeStep);
    ui->sbIntervals->setValue(exp.nOutInt);
    ui->leTime->setText(QString::number(exp.stepOutTime));

    // Populate tree
    populateTreeGeneric(ui->tree,mo.outputs,prj,COL_NAME,sEmpty,setItemTreeOut);

    // Check enabled buttons
    checkEnabled();
}

void outputs::checkEnabled()
{
    ui->sbIntervals->setEnabled(ui->rbIntervals->isChecked());
    ui->leTime->setEnabled(ui->rbTime->isChecked());
}

opt_exp outputs::getExp()
{
    return exp;
}

void outputs::on_buttonBox_accepted()
{
    // Save info in experiment
    exp.timeOutType = ui->rbIntervals->isChecked() ? ttTimeDist : ttTimeStep;
    exp.nOutInt     = ui->sbIntervals->value();
    exp.stepOutTime = ui->leTime->text().toDouble();

    // Accept button
    accept();
}

void outputs::on_rbIntervals_clicked()
{
    checkEnabled();
}

void outputs::on_rbTime_clicked()
{
    checkEnabled();
}
