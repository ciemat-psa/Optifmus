#ifndef OUTPUTS_H
#define OUTPUTS_H

#include <QDialog>
#include <QKeyEvent>

#include "opt_project.h"

namespace Ui {
class outputs;
}

class outputs : public QDialog
{
    Q_OBJECT

public:
    explicit outputs(QWidget *parent = 0);
    ~outputs();
    void keyPressEvent(QKeyEvent *e);
    void setModelExp(opt_project prj);
    opt_exp getExp();
    void checkEnabled();

protected:
    opt_exp   exp;
    opt_model mo;

private slots:
    void on_lineFilter_2_returnPressed();
    void on_tbClear_2_clicked();

    void on_buttonBox_accepted();

    void on_rbIntervals_clicked();

    void on_rbTime_clicked();

private:
    Ui::outputs *ui;
};

#endif // OUTPUTS_H
