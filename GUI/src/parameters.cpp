#include "parameters.h"
#include "ui_parameters.h"
#include "ui_conf.h"

#include <QSpinBox>
#include <QComboBox>
#include "commonapp.h"
#include "ui_conf.h"

const int WIDTH_NAME  = 100;
const int WIDTH_VALUE = 75;
const int WIDTH_DESC  = 250;
const int WIDTH_UNIT  = 60;
const int WIDTH_MIN   = 75;
const int WIDTH_MAX   = 75;

Parameters::Parameters(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Parameters)
{
    ui->setupUi(this);
    readGeometry(this,paramWin);

    setWindowTitle(tParamConfig);
    ui->lTitle->setText(tParameters);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->tree->setColumnWidth(PAR_COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(PAR_COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(PAR_COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(PAR_COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(PAR_COL_MIN,WIDTH_MIN);
    ui->tree->setColumnWidth(PAR_COL_MAX,WIDTH_MAX);
    ui->tree->setSortingEnabled(true);

    QStringList headerLabels;

    headerLabels.push_back(tName);
    headerLabels.push_back(tValue);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tMin);
    headerLabels.push_back(tMax);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    readState(ui->tree->header(),treeParam);
}

Parameters::~Parameters()
{
    writeGeometry(this,paramWin);
    writeState(ui->tree->header(),treeParam);
    delete ui;
}

void Parameters::on_buttonBox_accepted()
{
    accept();
}

void Parameters::on_buttonBox_rejected()
{
    reject();
}


void Parameters::populateTree(opt_project prj)
{
    // No changes yet in params
    for(int i=0;i<prj.getModel().params.count();i++)
    {
        changed.append(false);
        new_value.append(sEmpty);
    }
    populateTreeGeneric(ui->tree,prj.getModel().params,prj,PAR_COL_NAME,sEmpty,setItemTreeParams,lw);
}

void Parameters::on_le_textChanged(const QString &arg1)
{
    QLineEdit *qle = (QLineEdit *)sender();
    int pos;
    QString value;

    pos   = qle->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qle->property(PAR_PRO_VALUE.toLatin1().data()).toString();

    if (arg1 == value)
    {
        qle->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = arg1;
    }
    else
    {
        qle->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = arg1;
    }
}

void Parameters::on_sb_valueChanged(int arg1)
{
    QSpinBox *qsb = (QSpinBox *)sender();
    int pos;
    int value;

    pos   = qsb->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qsb->property(PAR_PRO_VALUE.toLatin1().data()).toInt();

    if (arg1 == value)
    {
        qsb->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = QString::number(arg1,NUM_FORMAT,NUM_DEC);
    }
    else
    {
        qsb->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = QString::number(arg1,NUM_FORMAT,NUM_DEC);
    }
}

void Parameters::on_cb_textChanged(const QString &arg1)
{
    QComboBox *qcb = (QComboBox *)sender();
    int pos;
    QString value;

    pos   = qcb->property(PAR_PRO_POS.toLatin1().data()).toInt();
    value = qcb->property(PAR_PRO_VALUE.toLatin1().data()).toString();

    if (arg1 == value)
    {
        qcb->setStyleSheet("color: black; font: normal");
        changed[pos]   = false;
        new_value[pos] = arg1;
    }
    else
    {
        qcb->setStyleSheet("color: green; font: bold");
        changed[pos]   = true;
        new_value[pos] = arg1;
    }
}

void Parameters::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void Parameters::on_toolButton_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void Parameters::on_lineFilter_returnPressed()
{
        filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void Parameters::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}
