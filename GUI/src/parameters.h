#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QDialog>
#include <QTreeWidgetItem>
#include "opt_project.h"
#include <QKeyEvent>

namespace Ui {
class Parameters;
}

class Parameters : public QDialog
{
    Q_OBJECT

public:
    explicit Parameters(QWidget *parent = 0);
    ~Parameters();
    void populateTree(opt_project prj);

    QList<bool> changed;
    QList<QString> new_value;

private slots:
    void keyPressEvent(QKeyEvent *e);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_le_textChanged(const QString &arg1);
    void on_sb_valueChanged(int arg1);
    void on_cb_textChanged(const QString &arg1);
    void on_toolButton_clicked();
    void on_lineFilter_returnPressed();
    void on_tbClear_clicked();

private:
    Ui::Parameters *ui;
    QList<QWidget *> lw;
};

#endif // PARAMETERS_H
