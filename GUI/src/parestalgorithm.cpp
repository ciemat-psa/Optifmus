#include "parestalgorithm.h"
#include "ui_parestalgorithm.h"

#include "ui_conf.h"
#include "commonapp.h"

#include <QSpinBox>
#include <QFileDialog>

const int COL_NAME    = 0;
const int COL_VALUE   = 1;
const int COL_VALUE2  = 2;
const int COL_LAST    = 3;

const int WIDTH_NAME   = 100;
const int WIDTH_VALUE  = 75;
const int WIDTH_VALUE2 = 75;
const int WIDTH_LAST   = 25;

const int DATA_INFO    = 1;
const int ITEM_ARG     = 2;
const int ITEM_WIDGET  = 3;
const int ITEM_WIDGET2 = 4;

const int ITEM_HEIGHT  = 30;

const QString pLINE_EDIT  = "LINE_EDIT";
const QString pFILE_OPEN  = "FILE_OPEN";
const QString pWIDGET_ARG = "WIDGET_ARG";

const QString cQToolButton = "QToolButton";
const QString cQComboBox   = "QComboBox";
const QString cQSpinBox    = "QSpinBox";
const QString cQLineEdit   = "QLineEdit";

parEstAlgorithm::parEstAlgorithm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parEstAlgorithm)
{
    ui->setupUi(this);
    readGeometry(this,peaWin);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->lbiSearchDomain->setText(tSearchDomain);
    ui->lbiSearchMethod->setText(tSearchMethod);
    ui->lbiTypeConstraints->setText(tTypeConst);
    ui->lbiInfoProperty->setText(tInfoProperty);
    ui->libiInfoItem->setText(tInfoItem);

    ui->lseachDomain->setStyleSheet(clBlue);
    ui->lsearchMethod->setStyleSheet(clBlue);
    ui->ltypeConstraint->setStyleSheet(clBlue);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_VALUE2,WIDTH_VALUE2);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);

    QStringList headerLabels;

    headerLabels.push_back(tProperty);
    headerLabels.push_back(tValue);
    headerLabels.push_back(sEmpty);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    readState(ui->tree->header(),treePea);

    getParameterEstimationList(algs);
    populateAlgorithms();

    ui->teAlgorithm->setOpenExternalLinks(true);

    ui->tree->setFocus();
}

parEstAlgorithm::~parEstAlgorithm()
{
    for(int i=0;i<algs.count();i++)
        deleteArgs(algs[i].getArgs());
    writeGeometry(this,peaWin);
    writeState(ui->tree->header(),treePea);
    delete ui;
}

void parEstAlgorithm::populateAlgorithms()
{
    bool oldState = ui->cbAlgorithm->blockSignals(true);
            
    ui->cbAlgorithm->clear();            
    for (int i=0;i<algs.count();i++)
        ui->cbAlgorithm->insertItem(i,algs[i].getMethod(),algs[i].getId());
    
    ui->cbAlgorithm->blockSignals(oldState);
    on_cbAlgorithm_currentIndexChanged(ui->cbAlgorithm->currentIndex());
}

void parEstAlgorithm::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void parEstAlgorithm::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void parEstAlgorithm::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void parEstAlgorithm::setAlgInterface(opt_method m)
{
    QString sm,sd,tc;

    sm = m.getSearchMethod() == smGradient ? tGradient : tFreeDerivative;
    sd = m.getSearchGlobal() == sgGlobal ? tGlobal : tLocal;

    if (m.getSearchMethod() == smGradient)
    {
        sm = sm + comma + space + (m.getRequiresGradient() ? tGradientReq : tGradientNoReq);
        sm = sm + comma + space + (m.getRequiresHessian()  ? tHessianReq  : tHessianNoReq);
    }

    switch (m.getTypeConstraints())
    {
        case tcUnconstraint: tc = tUnconstraint; break;
        case tcBound:        tc = tBoundCons;    break;
        case tcLinear:       tc = tLinear;       break;
        case tcNonLinear:    tc = tNonlinear;    break;
    }

    ui->lsearchMethod->setText(sm);
    ui->lseachDomain->setText(sd);
    ui->ltypeConstraint->setText(tc);
    ui->teAlgorithm->setText(m.getDesc());
    populateTreeAlg(m);
}

void parEstAlgorithm::on_cbAlgorithm_currentIndexChanged(int index)
{
    setAlgInterface(algs[index]);
}

bool isItemChecked(QTreeWidgetItem *item)
{
    return (!item->data(COL_NAME,Qt::CheckStateRole).isValid() && item->flags().testFlag(Qt::ItemIsEnabled)) ||
           (item->data(COL_NAME,Qt::CheckStateRole).isValid()  && item->checkState(COL_NAME) == Qt::Checked);
}

void parEstAlgorithm::itemArgInteger(opt_method_arg_integer *arg, QTreeWidgetItem *item, int COLUMN, int IWIDGET)
{
    QSpinBox *sb = new QSpinBox(ui->tree);

    sb->setEnabled(isItemChecked(item));
    sb->setMinimum(arg->getMin());
    sb->setMaximum(arg->getMax());
    sb->setValue(arg->getValue());
    sb->setProperty(pWIDGET_ARG.toStdString().c_str(),qVariantFromValue((void *)arg));
    sb->connect(sb,SIGNAL(valueChanged(int)),this,SLOT(on_sb_changed(int)));
    ui->tree->setItemWidget(item,COLUMN,sb);
    item->setData(IWIDGET,Qt::UserRole,qVariantFromValue((void *)sb));
}

void parEstAlgorithm::itemArgReal(opt_method_arg_real *arg, QTreeWidgetItem *item, int COLUMN, int IWIDGET)
{
    QLineEdit *le = new QLineEdit(ui->tree);

    le->setEnabled(isItemChecked(item));
    le->setValidator(new QDoubleValidator(arg->getMin(),arg->getMax(),NUM_DEC,ui->tree));
    le->setText(QString::number(arg->getValue()));
    le->setProperty(pWIDGET_ARG.toStdString().c_str(),qVariantFromValue((void *)arg));
    le->connect(le,SIGNAL(textChanged(QString)),this,SLOT(on_le_changed(QString)));
    ui->tree->setItemWidget(item,COLUMN,le);
    item->setData(IWIDGET,Qt::UserRole,qVariantFromValue((void *)le));
}

void parEstAlgorithm::itemArgStruct(opt_method_arg_struct *arg, QTreeWidgetItem *item, int COLUMN)
{
    for(int i=0;i<arg->getArgs().count();i++)
    {
        opt_method_arg *narg = arg->getArgs()[i];

        newItemTreeAlg(narg,item,COLUMN);
    }
}

void parEstAlgorithm::itemArgSet(opt_method_arg_set *arg, QTreeWidgetItem *item)
{
    QComboBox *cb = new QComboBox(ui->tree);
    int index     = 0;

    cb->setEnabled(isItemChecked(item));
    cb->setProperty(pWIDGET_ARG.toStdString().c_str(),qVariantFromValue((void *)arg));
    for(int i=0; i<arg->getOptions().count();i++)
    {
        opt_method_arg *opt = arg->getOptions()[i];

        opt->item = (void *)item;
        cb->addItem(opt->getArgument());
        cb->setItemData(i,qVariantFromValue((void *)opt));
        if (opt->getId() == arg->getValue()) index = i;
    }
    cb->connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(on_cb_currentIndexChanged(int)));
    ui->tree->setItemWidget(item,COL_VALUE,cb);
    item->setData(ITEM_WIDGET,Qt::UserRole,qVariantFromValue((void *)cb));
    cb->setCurrentIndex(index);
}

void parEstAlgorithm::itemArgFilename(opt_method_arg_filename *arg, QTreeWidgetItem *item, int COLUMN, int IWIDGET)
{
    QFrame      *frame = new QFrame(ui->tree);
    QLineEdit   *le    = new QLineEdit(ui->tree);
    QToolButton *tb    = new QToolButton(ui->tree);

    frame->setEnabled(isItemChecked(item));
    frame->setMaximumHeight(ITEM_HEIGHT);
    le->setReadOnly(true);
    le->setText(arg->getValue());
    tb->setIcon(QIcon(iLoadProject));
    tb->setToolButtonStyle(Qt::ToolButtonIconOnly);
    tb->connect(tb,SIGNAL(clicked(bool)),this,SLOT(on_tb_clicked(bool)));
    tb->setProperty(pLINE_EDIT.toStdString().c_str(),qVariantFromValue((void *)le));
    tb->setProperty(pFILE_OPEN.toStdString().c_str(),arg->getOpen());
    tb->setProperty(pWIDGET_ARG.toStdString().c_str(),qVariantFromValue((void *)arg));
    frame->setLayout(new QHBoxLayout());
    frame->layout()->addWidget(le);
    frame->layout()->addWidget(tb);
    ui->tree->setItemWidget(item,COLUMN,frame);
    item->setData(IWIDGET,Qt::UserRole,qVariantFromValue((void *)frame));
}

void defArgReal(opt_method_arg_real *arg, QLineEdit *le)
{
    le->setText(QString::number(arg->getDef()));
}

void defArgInt(opt_method_arg_integer *arg, QSpinBox *sb)
{
    sb->setValue(arg->getDef());
}

void defArgSet(opt_method_arg_set *arg, QComboBox *cb)
{
    bool found = false;
    int  i = 0;

    while (!found && i<arg->getOptions().count())
    {
        opt_method_arg *opt = arg->getOptions()[i];

        found = opt->getId() == arg->getDef();
        if (!found) i++;
    }
    cb->setCurrentIndex(i);
}

void defArgFilename(opt_method_arg_filename *arg, QFrame *frame)
{
    for(int i=0;i<frame->children().count();i++)
    {
        if (frame->children()[i]->inherits(cQLineEdit.toStdString().c_str()))
        {
            QLineEdit *le = (QLineEdit *)frame->children()[i];
            le->setText(arg->getDef());
        }
    }

}

void restoreDefault(QTreeWidgetItem *item)
{
    if (item->data(ITEM_ARG,Qt::UserRole).isValid() &&
        item->data(ITEM_WIDGET,Qt::UserRole).isValid())
    {
        opt_method_arg *arg  = (opt_method_arg *)item->data(ITEM_ARG,Qt::UserRole).value<void *>();
        QWidget        *w    = (QWidget *)item->data(ITEM_WIDGET,Qt::UserRole).value<void *>();
        unsigned        type = arg->type();

        switch(type)
        {
            case arg_keyword:  break; // Nothing to do
            case arg_struct:   break; // Nothing to do
            case arg_real:     defArgReal((opt_method_arg_real *)arg,(QLineEdit *)w); break;
            case arg_integer:  defArgInt((opt_method_arg_integer *)arg,(QSpinBox *)w); break;
            case arg_subset:   defArgSet((opt_method_arg_set *)arg,(QComboBox *)w); break;
            case arg_filename: defArgFilename((opt_method_arg_filename *)arg,(QFrame *)w);
            case arg_string:   break; // Not used
            case arg_bool:     break; // Not used
        }
    }

}

void enableItem(QTreeWidgetItem *item, bool enabled)
{
    if(enabled)
        // Enable item
        item->setFlags(item->flags()|Qt::ItemIsEnabled);
    else
    {
        // Disable item
        item->setFlags(item->flags()&~Qt::ItemIsEnabled);
        if (item->data(COL_NAME,Qt::CheckStateRole).isValid())
        {
            // Unckeck
            item->setCheckState(COL_NAME,Qt::Unchecked);
            // Restore default value
            restoreDefault(item);
        }
    }
}

void setEnabledItem(QTreeWidgetItem *item)
{
    QTreeWidgetItem *parent = item->parent();

    if (parent!= NULL && parent->data(COL_NAME,Qt::CheckStateRole).isValid() &&
        parent->checkState(COL_NAME) == Qt::Unchecked)
            enableItem(item,false);
}

void parEstAlgorithm::on_tb_clicked(bool b)
{
    Q_UNUSED(b);

    if (sender()!= NULL && sender()->inherits(cQToolButton.toStdString().c_str()))
    {
        QToolButton *tb = (QToolButton *)sender();

        if (tb->property(pFILE_OPEN.toStdString().c_str()).isValid() &&
            tb->property(pLINE_EDIT.toStdString().c_str()).isValid())
        {
            bool       open  = tb->property(pFILE_OPEN.toStdString().c_str()).toBool();
            QLineEdit *le    = (QLineEdit *)tb->property(pLINE_EDIT.toStdString().c_str()).value<void *>();
            QString    lfile = le->text();

            QString file = open ? QFileDialog::getOpenFileName(this, mSelectFile, "", ALLFilter):
                                  QFileDialog::getSaveFileName(this, mSelectFile, lfile, ALLFilter);
            if (!file.isEmpty())
            {
                le->setText(file);
                if (tb->property(pWIDGET_ARG.toStdString().c_str()).isValid())
                {
                    opt_method_arg_filename *arg = (opt_method_arg_filename *)tb->property(pWIDGET_ARG.toStdString().c_str()).value<void *>();
                    arg->setValue(file);
                }
            }
        }
    }
}

void parEstAlgorithm::on_sb_changed(int i)
{
    if (sender()!=NULL && sender()->inherits(cQSpinBox.toStdString().c_str()))
    {
        QSpinBox *sb = (QSpinBox *)sender();

        if (sb->property(pWIDGET_ARG.toStdString().c_str()).isValid())
        {
            opt_method_arg_integer *arg = (opt_method_arg_integer *)sb->property(pWIDGET_ARG.toStdString().c_str()).value<void *>();
            arg->setValue(i);
        }
    }
}

void parEstAlgorithm::on_le_changed(QString s)
{
    if (sender()!=NULL && sender()->inherits(cQLineEdit.toStdString().c_str()))
    {
        QLineEdit *le = (QLineEdit *)sender();

        if (le->property(pWIDGET_ARG.toStdString().c_str()).isValid())
        {
            opt_method_arg_real *arg = (opt_method_arg_real *)le->property(pWIDGET_ARG.toStdString().c_str()).value<void *>();
            arg->setValue(s.toDouble());
        }
    }
}

opt_method parEstAlgorithm::getMethod() const
{
    return method;
}

void parEstAlgorithm::setMethod(const opt_method &value, unsigned type)
{
    bool oldState;
    QList<opt_method_arg *> args;

    ui->lTitle->setText(type == ptInputEst ? tInputEstAlg : tParEstAlg);
    setWindowTitle(type == ptInputEst ? tInputEst : tParamConfig);

    // Set method in class (copy)
    method.setId(value.getId());
    method.setDesc(value.getDesc());
    method.setRequiresGradient(value.getRequiresGradient());
    method.setRequiresHessian(value.getRequiresHessian());
    method.setInequalityCons(value.getInequalityCons());
    copyArgs(args,value.getArgs());
    deleteArgs(method.getArgs());
    method.setArgs(args);

    // Set method in combo box
    oldState = ui->cbAlgorithm->blockSignals(true);
    ui->cbAlgorithm->setCurrentText(method.getMethod());
    ui->cbAlgorithm->blockSignals(oldState);

    // Interface for method
    setAlgInterface(method);
}

void parEstAlgorithm::on_cb_currentIndexChanged(int index)
{
    if (sender() != NULL && sender()->inherits(cQComboBox.toStdString().c_str()))
    {
        QComboBox *cb = (QComboBox *)sender();

        // Save its value
        if (cb->property(pWIDGET_ARG.toStdString().c_str()).isValid() &&
            cb->itemData(index).isValid())
        {
            opt_method_arg_set *arg = (opt_method_arg_set *)cb->property(pWIDGET_ARG.toStdString().c_str()).value<void *>();
            opt_method_arg     *opt = (opt_method_arg *)cb->itemData(index).value<void *>();

            arg->setValue(opt->getId());
        }


        // Process item
        if (cb->itemData(index).isValid() && !cb->itemData(index).isNull())
        {

            opt_method_arg  *arg  = (opt_method_arg *)cb->itemData(index).value<void *>();
            unsigned         type = arg->type();
            QTreeWidgetItem *item = (QTreeWidgetItem *)arg->item;

            // Item description
            ui->teItem->setText(arg->getDesc());

            // Delete previous widget
            if (item->data(ITEM_WIDGET2,Qt::UserRole).isValid() &&
               !item->data(ITEM_WIDGET2,Qt::UserRole).isNull())
            {
                QWidget *w = (QWidget *)item->data(ITEM_WIDGET2,Qt::UserRole).value<void *>();

                ui->tree->removeItemWidget(item,COL_VALUE2);
                delete w;
                item->setData(ITEM_WIDGET2,Qt::UserRole,QVariant(QString()));
            }

            // Delete childrens
            while (item->childCount()>0)
                item->removeChild(item->child(0));

            // Check kind of argument
            switch(type)
            {
                case arg_keyword:  break; // Nothing to consider
                case arg_real:     itemArgReal((opt_method_arg_real *)arg,item,COL_VALUE2,ITEM_WIDGET2); break;
                case arg_integer:  itemArgInteger((opt_method_arg_integer *)arg,item,COL_VALUE2,ITEM_WIDGET2); break;
                case arg_struct:   itemArgStruct((opt_method_arg_struct *)arg,item, COL_VALUE); item->setExpanded(true); break;
                case arg_filename: itemArgFilename((opt_method_arg_filename *)arg,item,COL_VALUE2,ITEM_WIDGET2); break;
                case arg_bool:     break; // Not used
                case arg_string:   break; // Not used
                case arg_subset:   break; // Not used
            }
        }
    }
}

QTreeWidgetItem *parEstAlgorithm::newItemTreeAlg(opt_method_arg *arg, QTreeWidgetItem *parent, int COLUMN)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    unsigned         type = arg->type();

    // Set argument name
    item->setText(COL_NAME,arg->getArgument());

    // Set checked
    if (arg->getOptional())
        item->setCheckState(COL_NAME,!arg->getOptional() ? Qt::Checked : arg->getChecked() ? Qt::Checked : Qt::Unchecked);

    // Set enabled
    setEnabledItem(item);

    // Insert item
    ui->tree->addTopLevelItem(item);

    // Set item info
    item->setData(DATA_INFO,Qt::UserRole,arg->getDesc());

    // Set item argument
    item->setData(ITEM_ARG,Qt::UserRole,qVariantFromValue((void *)arg));

    // Check kind of argument
    switch(type)
    {
        case arg_keyword:  break; // Nothing to consider
        case arg_real:     itemArgReal((opt_method_arg_real *)arg,item,COLUMN,ITEM_WIDGET); break;
        case arg_integer:  itemArgInteger((opt_method_arg_integer *)arg,item,COLUMN,ITEM_WIDGET); break;
        case arg_struct:   itemArgStruct((opt_method_arg_struct *)arg,item, COLUMN); break;
        case arg_subset:   itemArgSet((opt_method_arg_set *)arg,item); break;
        case arg_filename: itemArgFilename((opt_method_arg_filename *)arg,item,COLUMN,ITEM_WIDGET); break;
        case arg_string:   break; // Not used
        case arg_bool:     break; // Not used
    }
    return item;
}

void parEstAlgorithm::populateTreeAlg(opt_method m)
{
    QList<opt_method_arg *> args = m.getArgs();

    ui->tree->clear();
    ui->tree->setUpdatesEnabled(false);
    for (int i=0; i<args.count(); i++)
        newItemTreeAlg(args[i],NULL,COL_VALUE);
    ui->teItem->clear();
    ui->tree->setUpdatesEnabled(true);
}

void parEstAlgorithm::enableWidgets(QTreeWidgetItem *item, bool enabled)
{
    QWidget *w  = ui->tree->itemWidget(item,COL_VALUE);
    //QWidget *w2 = ui->tree->itemWidget(item,COL_VALUE2);

    if (w!=NULL)
    {
        w->setEnabled(enabled);
        if (!w->isEnabled()) restoreDefault(item);
    }

//    if (w2!=NULL)
//    {
//        qDebug() << w2->accessibleName();
//        w2->setEnabled(enabled);
//        if (!w2->isEnabled()) restoreDefault(item);
//    }
}

void parEstAlgorithm::on_tree_itemChanged(QTreeWidgetItem *item, int column)
{
    if (column == COL_NAME)
    {
        bool checked = !item->data(column,Qt::CheckStateRole).isValid() ||
                        item->checkState(COL_NAME) == Qt::Checked;

        if (item->data(ITEM_ARG,Qt::UserRole).isValid())
        {
            opt_method_arg *arg = (opt_method_arg *)item->data(ITEM_ARG,Qt::UserRole).value<void *>();
            arg->setChecked(item->checkState(COL_NAME) == Qt::Checked);
        }

        enableWidgets(item,checked);
        for (int i=0;i<item->childCount();i++)
        {
            bool enabledW = checked &&
                            (!item->child(i)->data(column,Qt::CheckStateRole).isValid() ||
                              item->child(i)->checkState(COL_NAME) == Qt::Checked);
            enableItem(item->child(i),checked);
            enableWidgets(item->child(i),enabledW);
        }
    }
}

void parEstAlgorithm::on_tree_itemActivated(QTreeWidgetItem *item, int column)
{
    if (column == COL_NAME)
    {
        // Clear item info
        ui->teItem->clear();

        // Data info
        if (item->data(DATA_INFO,Qt::UserRole).isValid())
            ui->teProperty->setText(item->data(DATA_INFO,Qt::UserRole).toString());

    }
}
