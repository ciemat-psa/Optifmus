#ifndef PARESTALGORITHM_H
#define PARESTALGORITHM_H

#include <QDialog>
#include <QKeyEvent>
#include <QTreeWidgetItem>

#include "dakota/dakota_methods.h"

namespace Ui {
class parEstAlgorithm;
}

class parEstAlgorithm : public QDialog
{
    Q_OBJECT

public:
    explicit parEstAlgorithm(QWidget *parent = 0);
    ~parEstAlgorithm();
    void keyPressEvent(QKeyEvent *e);
    void populateAlgorithms();
    void populateTreeAlg(opt_method m);
    QTreeWidgetItem *newItemTreeAlg(opt_method_arg *arg, QTreeWidgetItem *parent, int COLUMN);
    void setAlgInterface(opt_method m);

    void itemArgInteger(opt_method_arg_integer *arg, QTreeWidgetItem *item, int COLUMN,int IWIDGET);
    void itemArgReal(opt_method_arg_real *arg, QTreeWidgetItem *item, int COLUMN,int IWIDGET);
    void itemArgStruct(opt_method_arg_struct *arg, QTreeWidgetItem *item, int COLUMN);
    void itemArgFilename(opt_method_arg_filename *arg, QTreeWidgetItem *item, int COLUMN, int IWIDGET);
    void itemArgSet(opt_method_arg_set *arg, QTreeWidgetItem *item);
    void enableWidgets(QTreeWidgetItem *item, bool enabled);

    opt_method getMethod() const;
    void setMethod(const opt_method &value, unsigned type);

private slots:
    void on_lineFilter_returnPressed();
    void on_tbClear_clicked();
    void on_cbAlgorithm_currentIndexChanged(int index);   
    void on_tree_itemChanged(QTreeWidgetItem *item, int column);
    void on_tree_itemActivated(QTreeWidgetItem *item, int column);
    void on_cb_currentIndexChanged(int index);
    void on_tb_clicked(bool b);
    void on_sb_changed(int i);
    void on_le_changed(QString s);

private:
    Ui::parEstAlgorithm *ui;
    QList<opt_method> algs;
    opt_method method;
};

#endif // PARESTALGORITHM_H
