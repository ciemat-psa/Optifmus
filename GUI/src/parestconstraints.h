#ifndef PARESTCONSTRAINTS_H
#define PARESTCONSTRAINTS_H

#include <QDialog>
#include <QKeyEvent>
#include <QTableWidget>
#include <QLineEdit>

#include "opt_problem.h"
#include "opt_project.h"

// ---------------------------------------------------------------- //
// QLineEdit with focusInEvent reimplemented                        //
// ---------------------------------------------------------------- //
class MyLineEdit : public QLineEdit
{
  Q_OBJECT

signals:
  void focussed(MyLineEdit *le);

public:
  MyLineEdit(QWidget *parent = 0);
  ~MyLineEdit();

protected:
  virtual void focusInEvent(QFocusEvent *e);
};
// ---------------------------------------------------------------- //


namespace Ui {
class parestconstraints;
}

class parestconstraints : public QDialog
{
    Q_OBJECT

public:
    explicit parestconstraints(QWidget *parent = 0);
    ~parestconstraints();
    void setData(QList<opt_parameter> p, QList<variable> o, QList<opt_const_eq> leq, QList<opt_const_ie> lie, QList<opt_const_eq> nleq, QList<opt_const_ie> nlie, unsigned type);
    QList<opt_const_eq> getLinear_eq() const;
    QList<opt_const_ie> getLinear_ie() const;
    QList<opt_const_eq> getNon_linear_eq() const;
    QList<opt_const_ie> getNon_linear_ie() const;

private:
    Ui::parestconstraints *ui;
    QStringList param;
    QStringList output;
    QList<opt_const_eq> linear_eq;
    QList<opt_const_ie> linear_ie;
    QList<opt_const_eq> non_linear_eq;
    QList<opt_const_ie> non_linear_ie;
    void setEnabled();
    void addEqConsLinear(QTableWidget *t, opt_const_eq c, unsigned list_kind, unsigned list_pos);
    void addEqConsLinear(QTableWidget *t, opt_const_ie c, unsigned list_kind, unsigned list_pos);
    void addEqConsNonLinear(QTableWidget *t, opt_const_eq c, unsigned list_kind, unsigned list_pos);
    void addEqConsNonLinear(QTableWidget *t, opt_const_ie c, unsigned list_kind, unsigned list_pos);
    void configureTableLinear(QTableWidget *t, QStringList p, QList<opt_const_eq> l);
    void configureTableLinear(QTableWidget *t, QStringList p, QList<opt_const_ie> l);
    void configureTableNonLinear(QTableWidget *t, QStringList p, QList<opt_const_eq> l);
    void configureTableNonLinear(QTableWidget *t, QStringList p, QList<opt_const_ie> l);
    MyLineEdit *newLineEdit(QTableWidget *t, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, unsigned var_pos, double value, bool focus = false);
    void newComboBoxSymbol(QTableWidget *t, MyLineEdit *le, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, bool value, bool focus = false);
    void newComboBoxCons(QTableWidget *t, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, int value, bool focus = false);
    void newScaling(QTableWidget *t, int row, int col, unsigned sct, double scv,unsigned list_kind, unsigned list_pos);
    void showEquation(QWidget *w);
    bool showLinEq(int pos);
    bool showLinIe(int pos);
    bool showNonLinEq(int pos);
    bool showNonLinIe(int pos);
    bool validConstraints();

private slots:
    void keyPressEvent(QKeyEvent *e);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_tLinearEq_clicked(const QModelIndex &index);
    void on_bAddLinearEq_clicked();
    void on_bDeleteLinearEq_clicked();
    void on_le_textChanged(const QString &s);
    void cbSymbol_currentIndexChanged(int index);
    void cbCons_currentIndexChanged(int index);
    void cbSct_currentIndexChanged(int index);
    void focussed(MyLineEdit *le);
    void on_bAddLinearIe_clicked();
    void on_bDeleteLinearIe_clicked();
    void on_bAddNonLinearEq_clicked();
    void on_bDeleteNonLinearEq_clicked();
    void on_bAddNonLinearIe_clicked();
    void on_bDeleteNonLinearIe_clicked();
};

#endif // PARESTCONSTRAINTS_H
