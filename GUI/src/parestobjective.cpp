#include "parestobjective.h"
#include "ui_parestobjective.h"

#include <QPushButton>

#include "commonapp.h"
#include "ui_conf.h"

const int COL_NAME    = 0;
const int COL_DESC    = 1;
const int COL_UNIT    = 2;
const int COL_CRI     = 3;
const int COL_RED     = 4;
const int COL_WEI     = 5;
const int COL_SC_TYPE = 6;
const int COL_SC_VAL  = 7;
const int COL_LAST    = 8;

const int WIDTH_NAME    = 100;
const int WIDTH_DESC    = 250;
const int WIDTH_UNIT    = 60;
const int WIDTH_CRI     = 75;
const int WIDTH_RED     = 75;
const int WIDTH_WEI     = 75;
const int WIDTH_SC_TYPE = 75;
const int WIDTH_SC_VAL  = 75;
const int WIDTH_LAST    = 10;

const QString PRO_POS  = "PRO_POS";
const QString PRO_SCV  = "PRO_SCV";
const QString PRO_ITEM = "PRO_ITEM";

const unsigned W_CRI = 0;
const unsigned W_RED = 1;
const unsigned W_WEI = 2;
const unsigned W_SCT = 3;
const unsigned W_SCV = 4;
const unsigned OF_NAME = 5;

const unsigned DEF_CRI = crtMinimize;
const unsigned DEF_RED = redRMS;
const bool     DEF_UW  = false;
const double   DEF_WEI = 1;
const unsigned DEF_SCT = scNone;
const unsigned DEF_SCV = 1;

parestobjective::parestobjective(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestobjective)
{
    ui->setupUi(this);
    readGeometry(this,pefWin);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_CRI,WIDTH_CRI);
    ui->tree->setColumnWidth(COL_RED,WIDTH_RED);
    ui->tree->setColumnWidth(COL_WEI,WIDTH_WEI);
    ui->tree->setColumnWidth(COL_SC_TYPE,WIDTH_SC_TYPE);
    ui->tree->setColumnWidth(COL_SC_VAL,WIDTH_SC_VAL);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);

    QStringList headerLabels;

    headerLabels.push_back(tObjective);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tCriterion);
    headerLabels.push_back(tReduction);
    headerLabels.push_back(tWeight);
    headerLabels.push_back(tScalingType);
    headerLabels.push_back(tScalingValue);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    readState(ui->tree->header(),treePef);
}

parestobjective::~parestobjective()
{
    writeGeometry(this,pefWin);
    writeState(ui->tree->header(),treePef);
    delete ui;
}

void parestobjective::on_buttonBox_accepted()
{
    accept();
}

void parestobjective::on_buttonBox_rejected()
{
    reject();
}

void parestobjective::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void parestobjective::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void parestobjective::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void setEnabledW(QWidget *w, QTreeWidgetItem *item, bool additional = true)
{
    w->setEnabled(item->checkState(COL_NAME) == Qt::Checked && additional);
}

void setNameObjFunc(QObject *w, QString name)
{
    w->setProperty(PRO_POS.toStdString().c_str(),name);
}

QString getNameObjFunc(QObject *w)
{
    if (w==NULL) return sEmpty;
    return w->property(PRO_POS.toStdString().c_str()).toString();
}

void setScalingValue(QObject *w, QLineEdit *le)
{
    w->setProperty(PRO_SCV.toStdString().c_str(),qVariantFromValue((void *)le));
}

QLineEdit *getScalintValue(QObject *w)
{
    if (w==NULL) return NULL;
    return (QLineEdit *)w->property(PRO_SCV.toStdString().c_str()).value<void *>();
}

void setItem(QObject *w, QTreeWidgetItem *item)
{
    w->setProperty(PRO_ITEM.toStdString().c_str(),qVariantFromValue((void *)item));
}

QTreeWidgetItem *getItem(QObject *w)
{
    if (w==NULL) return NULL;
    return (QTreeWidgetItem *)w->property(PRO_ITEM.toStdString().c_str()).value<void *>();
}




int findObjective(QList<opt_objfnc> of, QString name)
{
    bool found = false;
    int  i     = 0;

    while (!found && i<of.count())
    {
        found = of[i].getName() == name;
        if (!found) i++;
    }
    return found ? i : -1;
}

void parestobjective::on_criteria_indexChanged(int index)
{
    QString name = getNameObjFunc(sender());
    if (name != sEmpty)
    {
        int pos = findObjective(of,name);
        if (pos>=0) of[pos].setCriteria(index);
    }
}

void parestobjective::on_reduction_indexChanged(int index)
{
    QString name = getNameObjFunc(sender());
    if (name != sEmpty)
    {
        int pos = findObjective(of,name);
        if (pos>=0) of[pos].setReduction(index);
    }
}

void parestobjective::on_weight_textChanged(const QString &arg)
{
    QString name = getNameObjFunc(sender());
    if (name != sEmpty)
    {
        int pos = findObjective(of,name);
        if (pos>=0) of[pos].setWeight(arg.toDouble());
    }
}

void parestobjective::on_sc_indexChanged(int index)
{
    QString name = getNameObjFunc(sender());
    if (name != sEmpty)
    {
        int pos = findObjective(of,name);
        if (pos>=0) of[pos].setScalingType(index);
    }

    QLineEdit       *scv  = getScalintValue(sender());
    QTreeWidgetItem *item = getItem(sender());

    if (scv!=NULL && item!=NULL)
        setEnabledW(scv,item,(index == scLog || index == scValue));
}

void parestobjective::on_scv_textChanged(const QString &arg)
{
    QString name = getNameObjFunc(sender());
    if (name != sEmpty)
    {
        int pos = findObjective(of,name);
        if (pos>=0) of[pos].setScalingValue(arg.toDouble());
    }
}

void populateCriteriaCombo(QComboBox *cb)
{
    cb->clear();
    cb->insertItem(crtMinimize,crttMinimize);
    cb->insertItem(crtMaximize,crttMaximize);
}

void populateScalingComboObjFun(QComboBox *cb)
{
    cb->clear();
    cb->insertItem(scNone,sctNone);
    cb->insertItem(scValue,sctValue);
    cb->insertItem(scLog,sctLog);
    cb->insertItem(scAuto,sctAuto);
}

void populateReductionCombo(QComboBox *cb)
{
    cb->clear();
    cb->insertItem(redRMS, redtRMS);
    cb->insertItem(redSum, redtSum);
    cb->insertItem(redLast,redtLast);
    cb->insertItem(redMax ,redtMax);
    cb->insertItem(redMin ,redtMin);
}

void setItemTreeParEstObjective(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(v);
    Q_UNUSED(vs);
    Q_UNUSED(lw);

    opt_model         m  = prj.getModel();
    QList<opt_objfnc> of = prj.getPrb().getObjFncs();

    switch (m.outputs[k].type)
    {
        case FMI_REAL:
        {
            // Widgets
            QComboBox *cbCriteria  = new QComboBox(tree);
            QComboBox *cbReduction = new QComboBox(tree);
            QLineEdit *leWeight    = new QLineEdit(tree);
            QComboBox *cbsct       = new QComboBox(tree);
            QLineEdit *lescv       = new QLineEdit(tree);

            // Item info
            item->setIcon(COL_NAME,QIcon(iReal));
            item->setText(COL_DESC,m.outputs[k].desc);
            item->setText(COL_UNIT,formatedUnit(m,m.outputs,k));

            // Item widgets
            item->setData(W_CRI,Qt::UserRole,qVariantFromValue((void *)cbCriteria));
            item->setData(W_RED,Qt::UserRole,qVariantFromValue((void *)cbReduction));
            item->setData(W_WEI,Qt::UserRole,qVariantFromValue((void *)leWeight));
            item->setData(W_SCT,Qt::UserRole,qVariantFromValue((void *)cbsct));
            item->setData(W_SCV,Qt::UserRole,qVariantFromValue((void *)lescv));
            item->setData(OF_NAME,Qt::UserRole,m.outputs[k].name);

            // Widget values
            int pos = findObjective(of,m.outputs[k].name);

            unsigned crt = pos>=0 ? of[pos].getCriteria()      : DEF_CRI;
            unsigned red = pos>=0 ? of[pos].getReduction()     : DEF_RED;
            bool     awe = pos>=0 ? of[pos].getUseWeight()     : DEF_UW;
            double   wei = pos>=0 ? of[pos].getWeight()        : DEF_WEI;
            unsigned sct = pos>=0 ? of[pos].getScalingType()   : DEF_SCT;
            double   scv = pos>=0 ? of[pos].getScalingValue()  : DEF_SCV;

            // Item checked
            item->setCheckState(COL_NAME, pos>=0 ? Qt::Checked : Qt::Unchecked);

            // Criteria widget
            setNameObjFunc(cbCriteria,m.outputs[k].name);
            setEnabledW(cbCriteria,item);
            populateCriteriaCombo(cbCriteria);
            cbCriteria->setCurrentIndex(crt);
            cbCriteria->connect(cbCriteria,SIGNAL(currentIndexChanged(int)),tree->parent(),SLOT(on_criteria_indexChanged(int)));
            tree->setItemWidget(item,COL_CRI,cbCriteria);

            // Reduction widget
            setNameObjFunc(cbReduction,m.outputs[k].name);
            setEnabledW(cbReduction,item);
            populateReductionCombo(cbReduction);
            cbReduction->setCurrentIndex(red);
            cbReduction->connect(cbReduction,SIGNAL(currentIndexChanged(int)),tree->parent(),SLOT(on_reduction_indexChanged(int)));
            tree->setItemWidget(item,COL_RED,cbReduction);

            // Weight widget
            setNameObjFunc(leWeight,m.outputs[k].name);
            setEnabledW(leWeight,item,awe);
            setValidator(leWeight,0,MAX_DOUBLE);
            leWeight->setText(QString::number(wei));
            leWeight->connect(leWeight,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_weight_textChanged(const QString &)));
            tree->setItemWidget(item,COL_WEI,leWeight);

            // Scaling type
            setNameObjFunc(cbsct,m.outputs[k].name);
            setScalingValue(cbsct,lescv);
            setItem(cbsct,item);
            setEnabledW(cbsct,item);
            populateScalingComboObjFun(cbsct);
            cbsct->setCurrentIndex(sct);
            cbsct->connect(cbsct,SIGNAL(currentIndexChanged(int)),tree->parent(),SLOT(on_sc_indexChanged(int)));
            tree->setItemWidget(item,COL_SC_TYPE,cbsct);

            // Scaling value
            setNameObjFunc(lescv,m.outputs[k].name);
            setEnabledW(lescv,item,(sct == scLog || sct == scValue));
            setValidator(lescv,0,MAX_DOUBLE);
            lescv->setText(QString::number(scv));
            lescv->connect(lescv,SIGNAL(textChanged(const QString &)),tree->parent(),SLOT(on_scv_textChanged(const QString &)));
            tree->setItemWidget(item,COL_SC_VAL,lescv);

            break;
        }
        case FMI_INTEGER: {break;}
        case FMI_STRING:  {break;}
        case FMI_BOOL:    {break;}
        default:          {break;}
    }
}

void parestobjective::setProject(opt_project prj)
{
    ui->lTitle->setText(prj.getType() == ptInputEst ? tInputEstObj : tParEstObjFun);
    setWindowTitle(prj.getType() == ptInputEst ? tInputEst : tParamConfig);

    m  = prj.getModel();
    of = prj.getPrb().getObjFncs();

    ui->cbApplyWeights->setChecked(of.count()>0 && of[0].getUseWeight());
    bool oldState = ui->tree->blockSignals(true);
    populateTreeGeneric(ui->tree,m.outputs,prj,COL_NAME,sEmpty,setItemTreeParEstObjective,lw);
    ui->tree->blockSignals(oldState);
    checkEnabled();
}

QList<opt_objfnc> parestobjective::getObjFncs()
{
    return of;
}

void parestobjective::checkEnabled()
{
    bool valid = of.count()>0;
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(valid);
}

void parestobjective::on_tree_itemChanged(QTreeWidgetItem *item, int column)
{
    if (column == COL_NAME)
    {
        bool checked = !item->data(column,Qt::CheckStateRole).isValid() ||
                        item->checkState(COL_NAME) == Qt::Checked;

        QComboBox *cbCriteria  = (QComboBox *)item->data(W_CRI,Qt::UserRole).value<void *>();
        QComboBox *cbReduction = (QComboBox *)item->data(W_RED,Qt::UserRole).value<void *>();
        QLineEdit *leWeight    = (QLineEdit *)item->data(W_WEI,Qt::UserRole).value<void *>();
        QComboBox *csct        = (QComboBox *)item->data(W_SCT,Qt::UserRole).value<void *>();
        QLineEdit *lscv        = (QLineEdit *)item->data(W_SCV,Qt::UserRole).value<void *>();
        QString    name        = item->data(OF_NAME,Qt::UserRole).toString();

        if (checked)
        {
            of.append(opt_objfnc(name,FMI_REAL,cbCriteria->currentIndex(),cbReduction->currentIndex(),ui->cbApplyWeights->checkState() == Qt::Checked,
                                 leWeight->text().toDouble(),csct->currentIndex(),lscv->text().toDouble()));
            setNameObjFunc(cbCriteria, name);
            setNameObjFunc(cbReduction,name);
            setNameObjFunc(leWeight,   name);
            setNameObjFunc(csct,       name);
            setNameObjFunc(lscv,       name);
        }
        else
        {
            setNameObjFunc(cbCriteria, sEmpty);
            setNameObjFunc(cbReduction,sEmpty);
            setNameObjFunc(leWeight,   sEmpty);
            setNameObjFunc(csct,       sEmpty);
            setNameObjFunc(lscv,       sEmpty);

            cbCriteria->setCurrentIndex(DEF_CRI);
            cbReduction->setCurrentIndex(DEF_RED);
            leWeight->setText(QString::number(DEF_WEI));
            csct->setCurrentIndex(DEF_SCT);
            lscv->setText(QString::number(DEF_SCV));

            int pos = findObjective(of,name);
            if (pos>=0) of.removeAt(pos);
        }

        setEnabledW(cbCriteria,item);
        setEnabledW(cbReduction,item);
        setEnabledW(leWeight,item,ui->cbApplyWeights->checkState() == Qt::Checked);
        setEnabledW(csct,item);
        setEnabledW(lscv,item,(csct->currentIndex() == scLog ||
                               csct->currentIndex() == scValue));
        checkEnabled();
    }
}

void parestobjective::setEnabledWeights(QTreeWidgetItem *item, bool enabled)
{
    if (item->childCount()>0)
    {
        for(int i=0;i<item->childCount();i++)
            setEnabledWeights(item->child(i),enabled);
    }
    else
    {
        QLineEdit *leWeight = (QLineEdit *)item->data(W_WEI,Qt::UserRole).value<void *>();
        setEnabledW(leWeight,item,enabled);
    }
}

void parestobjective::on_cbApplyWeights_clicked()
{
    for(int i=0;i<of.count();i++)
        of[i].setUseWeight(ui->cbApplyWeights->checkState() == Qt::Checked);
    for (int i=0;i<ui->tree->topLevelItemCount();i++)
    setEnabledWeights(ui->tree->topLevelItem(i),ui->cbApplyWeights->checkState() == Qt::Checked);
}
