#ifndef PARESTOBJECTIVE_H
#define PARESTOBJECTIVE_H

#include <QDialog>
#include <QKeyEvent>
#include <QTreeWidgetItem>

#include "opt_project.h"
#include "opt_problem.h"

namespace Ui {
class parestobjective;
}

class parestobjective : public QDialog
{
    Q_OBJECT

public:
    explicit parestobjective(QWidget *parent = 0);
    ~parestobjective();
    void setProject(opt_project prj);
    QList<opt_objfnc> getObjFncs();
    void checkEnabled();
    void setEnabledWeights(QTreeWidgetItem *item, bool enabled);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_lineFilter_returnPressed();
    void on_tbClear_clicked();
    void keyPressEvent(QKeyEvent *e);
    void on_criteria_indexChanged(int index);
    void on_reduction_indexChanged(int index);
    void on_weight_textChanged(const QString &arg);
    void on_sc_indexChanged(int index);
    void on_scv_textChanged(const QString &arg);
    void on_tree_itemChanged(QTreeWidgetItem *item, int column);

    void on_cbApplyWeights_clicked();

private:
    Ui::parestobjective *ui;
    opt_model            m;
    QList<opt_objfnc>    of;
    QList<QWidget *>     lw;
};

#endif // PARESTOBJECTIVE_H
