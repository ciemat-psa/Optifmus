#ifndef PARESTPLOT_H
#define PARESTPLOT_H

#include <QDialog>
#include <QMenu>

#include "opt_problem.h"

namespace Ui {
class parestplot;
}

class parestplot : public QDialog
{
    Q_OBJECT

public:
    explicit parestplot(QWidget *parent = 0);
    ~parestplot();
    void plot2D(opt_result x, opt_result y);

private slots:
    void on_actionExport_to_file_triggered();
    void on_chartview_customContextMenuRequested(const QPoint &pos);

    void on_actionCopy_to_clipboard_triggered();

    void on_bFile_clicked();

    void on_bClipboard_clicked();

private:
    Ui::parestplot *ui;
    QMenu cmGraph;
};

#endif // PARESTPLOT_H
