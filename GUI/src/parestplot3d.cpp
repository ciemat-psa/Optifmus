#include "parestplot3d.h"
#include "ui_parestplot3d.h"

#include <QFileDialog>

#include "ui_conf.h"
#include "commonapp.h"

using namespace QtDataVisualization;

parestplot3d::parestplot3d(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestplot3d)
{
    ui->setupUi(this);
    readGeometry(this,p3DWin);

    setWindowTitle(t3DScatterGrp);

    const QIcon icClipboard = QIcon(iClipboard);
    const QIcon icFile      = QIcon(iFile);

    ui->actionExport_to_file->setIcon(icFile);
    ui->actionCopy_to_clipboard->setIcon(icClipboard);

    ui->bFile->setIcon(icFile);
    ui->bClipboard->setIcon(icClipboard);

    cmGraph.addAction(ui->actionExport_to_file);
    cmGraph.addAction(ui->actionCopy_to_clipboard);

    graph     = new Q3DScatter();
    container = QWidget::createWindowContainer(graph);
    proxy     = new QScatterDataProxy;
    series    = new QScatter3DSeries(proxy);
    dataArray = new QScatterDataArray;

    this->layout()->addWidget(container);

    series->setBaseColor(QColor(32,59,223));
    series->setItemSize(0.15);

    graph->setFlags(graph->flags() ^ Qt::FramelessWindowHint);
    graph->activeTheme()->setType(Q3DTheme::ThemeQt);
    graph->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);
    graph->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetIsometricLeftHigh);
    graph->addSeries(series);
    graph->setModality(Qt::WindowModal);

    container->setFocus();
}

parestplot3d::~parestplot3d()
{
    writeGeometry(this,p3DWin);
    delete container;
    delete ui;
}

void parestplot3d::plot3D(opt_result x, opt_result y, opt_result z)
{
    graph->axisX()->setTitleVisible(true);
    graph->axisY()->setTitleVisible(true);
    graph->axisZ()->setTitleVisible(true);
    graph->axisX()->setTitle(x.getName());
    graph->axisY()->setTitle(y.getName());
    graph->axisZ()->setTitle(z.getName());

    QString cad = "(" + x.getName() + "," + y.getName() + "," + z.getName() + ") = (@xLabel, @yLabel, @zLabel)";
    series->setItemLabelFormat(cad);

    for(int i=0;i<x.getValues().count();i++)
        dataArray->append(QVector3D(x.getValues()[i],y.getValues()[i],z.getValues()[i]));

    graph->seriesList().at(0)->dataProxy()->resetArray(dataArray);
}

void parestplot3d::ShowContextMenu(const QPoint &pos)
{
   cmGraph.exec(container->mapToGlobal(pos));
}

void parestplot3d::on_actionExport_to_file_triggered()
{
    QFileDialog dlg(this, mResultParEstSave, sEmpty, PDFFilter + ";;" + SVGFilter + ";;" + PNGFilter + ";;" + BMPFilter + ";;" + JPGFilter + ";;" + JPEGFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
      if (dlg.selectedNameFilter() == PDFFilter)
          chartToPDF(graph,dlg.selectedFiles()[0]);
      else if (dlg.selectedNameFilter() == SVGFilter)
          chartToSVG(graph,dlg.selectedFiles()[0]);
      else
          chartToImage(graph,false,dlg.selectedFiles()[0]);
    }
}

void parestplot3d::on_actionCopy_to_clipboard_triggered()
{
    chartToImage(graph);
}

void parestplot3d::on_bFile_clicked()
{
    ui->actionExport_to_file->trigger();
}

void parestplot3d::on_bClipboard_clicked()
{
    ui->actionCopy_to_clipboard->trigger();
}
