#ifndef PARESTPLOT3D_H
#define PARESTPLOT3D_H

#include <QDialog>
#include <QMenu>
#include <QtDataVisualization/QtDataVisualization>
#include <QtDataVisualization/Q3DScatter>
#include <QHBoxLayout>

#include "opt_problem.h"

using namespace QtDataVisualization;

namespace Ui {
class parestplot3d;
}

class parestplot3d : public QDialog
{
    Q_OBJECT

public:
    explicit parestplot3d(QWidget *parent = 0);
    ~parestplot3d();
    void plot3D(opt_result x, opt_result y, opt_result z);

private slots:
    void on_actionExport_to_file_triggered();
    void on_actionCopy_to_clipboard_triggered();
    void ShowContextMenu(const QPoint &pos);

    void on_bFile_clicked();

    void on_bClipboard_clicked();

private:
    Ui::parestplot3d *ui;
    QMenu cmGraph;
    Q3DScatter        *graph;
    QWidget           *container;
    QScatterDataProxy *proxy;
    QScatter3DSeries  *series;
    QScatterDataArray *dataArray;
};

#endif // PARESTPLOT3D_H
