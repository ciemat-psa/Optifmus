#include "qfmioptmain.h"
#include "ui_qfmioptmain.h"

#include <QToolButton>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QLocale>
#include <QtCharts/QtCharts>

using namespace QtCharts;

#include <JlCompress.h>

#include "ui_conf.h"
#include "parameters.h"
#include "integrator.h"
#include "commonapp.h"
#include "experiment.h"
#include "modelstructure.h"
#include "outputs.h"
#include "parestalgorithm.h"
#include "parestparameters.h"
#include "parest.h"
#include "parestobjective.h"
#include "parestresults.h"
#include "parestconstraints.h"
#include "fmuinfo.h"
#include "confapp.h"
#include "options.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include "import/base/include/FMUModelExchange_v2.h"

const int     tabSimulation = 0;
const int     tabEstimation = 1;
const int     tabInputEst   = 2;
const QString PB_TIME       = "PB_TIME";

// Dynamic widgets
QMenu       *menu;
QToolButton *toolButton;

QFMIOptMain::QFMIOptMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QFMIOptMain)
{
    // ------------------------------------------ //
    // Register meta types for signal/slots       //
    // ------------------------------------------ //
    qRegisterMetaType<opt_exp>("opt_exp");
    qRegisterMetaType<opt_problem>("opt_problem");
    // ------------------------------------------ //
    thrSim = NULL;
    thread = NULL;
    prj    = NULL;
    ui->setupUi(this);
    readGeometry(this,mainWin);
    defaultProject();
    setInterfaceConf();
    setInterfaceDefault();
    readState(ui->treeLog->header(),treeLog);
    setState();

    // Recent projects
    ui->menuRecent_project->setVisible(false);
    ui->menuRecent_project->setEnabled(false);

    // Text editor
    textEditor = readOption(optTextEditor,defTextEditor);

    // Button bar
    ui->mainToolBar->setHidden(readOption(optToolBar,defToolBar));
    connect(ui->mainToolBar,SIGNAL(visibilityChanged(bool)),SLOT(toolBarVisibility(bool)));

    // Recent files
    for (int i = 0; i < MaxRecentFiles; ++i)
    {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        connect(recentFileActs[i], SIGNAL(triggered()),this, SLOT(open_recent_project()));
    }

    // Create menus
    for (int i = 0; i < MaxRecentFiles; ++i)
            ui->menuRecent_project->addAction(recentFileActs[i]);
}

QFMIOptMain::~QFMIOptMain()
{
    for (int i = 0; i < MaxRecentFiles; ++i)
        delete recentFileActs[i];
    writeGeometry(this,mainWin);
    writeState(ui->treeLog->header(),treeLog);
    ui->treeLog->clear();
    delete menu;
    delete toolButton;
    prj->deletePrbMethodArgs();
    delete prj;
    delete ui;
}

QString strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}

void QFMIOptMain::updateRecentFileActions()
{
    QSettings settings(company,appName);
    QStringList files = settings.value("recentFileList").toStringList();

    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i) {
        QString text = tr("&%1 %2").arg(i + 1).arg(strippedName(files[i]));
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFileActs[j]->setVisible(false);

    ui->menuRecent_project->setEnabled(numRecentFiles>0);
}

void QFMIOptMain::setCurrentFile(const QString &fileName)
{
    QSettings settings(company,appName);
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(fileName);
    files.prepend(fileName);
    while (files.size() > MaxRecentFiles)
        files.removeLast();

    settings.setValue("recentFileList", files);

    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
        QFMIOptMain *mainWin = qobject_cast<QFMIOptMain *>(widget);
        if (mainWin) mainWin->updateRecentFileActions();
    }
}

void QFMIOptMain::toolBarVisibility(bool visible)
{
    writeOption(optToolBar,!visible);
}

void QFMIOptMain::defaultProject()
{
    newProject(ptSimulation);
}

void QFMIOptMain::on_actionExit_triggered()
{
    close();
}

void QFMIOptMain::on_bModelFile_clicked()
{
    QString path = fileAbsoluteFromProject(prj,prj->getModel().getFMUfileName());
    QString file = QFileDialog::getOpenFileName(this, mSelectFMU, path, FMUFilter);

    if (!file.isEmpty())
    {
        QFontMetrics fm(ui->leModelFile->font());
        QString newFile = fileRelativeToProject(prj,file);

        ui->leModelFile->setText(fm.elidedText(file,Qt::ElideMiddle,ui->leModelFile->width()-WIDTH_EPS));
        ui->leModelFile->setStatusTip(file);
        infoFMU(newFile);
    }
    setState();
}

void QFMIOptMain::setInterfaceDefault()
{
    // Clear info in GUI
    bool oldState = ui->leProjectName->blockSignals(true);
    ui->leProjectName->setText(sEmpty);
    ui->leProjectName->setStatusTip(sEmpty);
    ui->leProjectName->blockSignals(oldState);
    ui->leModelFile->setText(sEmpty);   
    ui->lbVersion->setText(sEmpty);
    ui->lbInputs->setText(sEmpty);
    ui->lbParameters->setText(sEmpty);
    ui->lbOutputs->setText(sEmpty);
    ui->lbStatus->setText(sEmpty);
    ui->lbME->setText(sEmpty);
    ui->lbCo->setText(sEmpty);
    ui->lbExpData->setText(sEmpty);
    ui->lbSimInt->setText("(0,1)");
    ui->lbIntegrator->setText(integratorToText(prj->getSim().integrator));
    ui->lbProgress->setText(QString::number(prj->getExp().startTime,NUM_FORMAT,NUM_DEC) + " / " +
                            QString::number(prj->getExp().stopTime,NUM_FORMAT,NUM_DEC)  + " (s)");
    ui->pbProgress->setValue(0);
    ui->pbProgress->setProperty(PB_TIME.toStdString().c_str(),0);
    ui->treeLog->clear();
    ui->textSimLog->clear();
    ui->textOptOut->clear();
    ui->textOptError->clear();
    ui->tabLog->setCurrentIndex(0);
    ui->tabOptLog->setCurrentIndex(0);
    ui->bFMU->setStyleSheet(sEmpty);

    // GUI configuration depeding on project type (last one is the default one)
    ui->tabWidget->setCurrentIndex(prj->getType() == ptInputEst   ? tabInputEst   :
                                   prj->getType() == ptEstimation ? tabEstimation : tabSimulation);

    // Title
    switch(prj->getType())
    {
        case ptSimulation : ui->lTitle->setText(tSimulation); break;
        case ptEstimation : ui->lTitle->setText(tParEst);     break;
        case ptInputEst   : ui->lTitle->setText(tInputEst);   break;
    }

}

void QFMIOptMain::setInterfaceConf()
{
    // Window configuration
    // --------------------
    const QIcon icApp = QIcon(iApp);
    setWindowIcon(icApp);

    // Interface icons
    // ---------------
    const QIcon icNewProject    = QIcon(iNewProject);
    const QIcon icLoadProject   = QIcon(iLoadProject);
    const QIcon icSaveProject   = QIcon(iSaveProject);
    const QIcon icSaveProjectAs = QIcon(iSaveProjectAs);
    const QIcon icExit          = QIcon(iExit);
    const QIcon icParameterCal  = QIcon(iParameterCal);
    const QIcon icOptions       = QIcon(iOptions);
    const QIcon icParameters    = QIcon(iParameters);
    const QIcon icModelFile     = QIcon(iModelFile);
    const QIcon icSimulate      = QIcon(iSimulate);
    const QIcon icRefresh       = QIcon(iRefresh);
    const QIcon icExperiment    = QIcon(iExperiment);
    const QIcon icIntegrator    = QIcon(iIntegrator2);
    const QIcon icStructure     = QIcon(iStructure);
    const QIcon icFMU           = QIcon(iFMU);
    const QIcon icOutputs       = QIcon(iOutputs);
    const QIcon icStop          = QIcon(iStop);
    const QIcon icResults       = QIcon(iResults);
    const QIcon icMessages      = QIcon(iMessages);
    const QIcon icLog           = QIcon(iLog);
    const QIcon icNewDocument   = QIcon(iNewProject);
    const QIcon icDocPreview    = QIcon(iDocPreview);
    //const QIcon icError         = QIcon(iError);
    const QIcon icLogOut        = QIcon(iLogOut);
    const QIcon icObjective     = QIcon(iObjective);
    const QIcon icCalibrate     = QIcon(iCalibrate);
    const QIcon icConstraints   = QIcon(iConstraints);
    const QIcon icCode          = QIcon(iCode);
    const QIcon icRecent        = QIcon(iRecent);
    const QIcon icInputs        = QIcon(iInputs);

    // Menu and actions
    // ----------------
    ui->menuFile->setTitle(tFile);
    ui->menuFile->setToolTip(sEmpty);
    ui->menuFile->setStatusTip(sEmpty);

    ui->menu_New_Project->setTitle(tNewProject);
    ui->menu_New_Project->setToolTip(sEmpty);
    ui->menu_New_Project->setIcon(icNewProject);
    ui->menu_New_Project->setStatusTip(sEmpty);

    ui->actionExit->setText(tExit);
    ui->actionExit->setToolTip(sEmpty);
    ui->actionExit->setIcon(icExit);
    ui->actionExit->setStatusTip(sEmpty);

    ui->actionParameter_Calibration->setText(tParameterCal);
    ui->actionParameter_Calibration->setToolTip(sEmpty);
    ui->actionParameter_Calibration->setIcon(icParameterCal);
    ui->actionParameter_Calibration->setStatusTip(sEmpty);

    ui->actionSimulation->setText(tSimulation);
    ui->actionSimulation->setToolTip(sEmpty);
    ui->actionSimulation->setIcon(icParameterCal);
    ui->actionSimulation->setStatusTip(sEmpty);

    ui->actionInput_estimation->setText(tInputEst);
    ui->actionInput_estimation->setToolTip(sEmpty);
    ui->actionInput_estimation->setIcon(icParameterCal);
    ui->actionInput_estimation->setStatusTip(sEmpty);

    ui->actionSave_project->setText(tSaveProject);
    ui->actionSave_project->setToolTip(sEmpty);
    ui->actionSave_project->setIcon(icSaveProject);
    ui->actionSave_project->setStatusTip(sEmpty);

    ui->actionSave_project_as->setText(tSaveProjectAs);
    ui->actionSave_project_as->setToolTip(sEmpty);
    ui->actionSave_project_as->setIcon(icSaveProjectAs);
    ui->actionSave_project_as->setStatusTip(sEmpty);

    ui->action_Load_project->setText(tLoadProject);
    ui->action_Load_project->setToolTip(sEmpty);
    ui->action_Load_project->setIcon(icLoadProject);
    ui->action_Load_project->setStatusTip(sEmpty);

    ui->actionExit->setText(tExit);
    ui->actionExit->setToolTip(sEmpty);
    ui->actionExit->setIcon(icExit);
    ui->actionExit->setStatusTip(sEmpty);

    ui->actionSimulate->setText(tSimulate);
    ui->actionSimulate->setToolTip(sEmpty);
    ui->actionSimulate->setIcon(icSimulate);
    ui->actionSimulate->setStatusTip(sEmpty);

    ui->actionOptions->setText(tOptions);
    ui->actionOptions->setToolTip(sEmpty);
    ui->actionOptions->setIcon(icOptions);
    ui->actionOptions->setStatusTip(sEmpty);

    ui->menuRecent_project->setTitle(tRecentProjects);
    ui->menuRecent_project->setToolTip(sEmpty);
    ui->menuRecent_project->setIcon(icRecent);
    ui->menuRecent_project->setStatusTip(sEmpty);

    // Group boxes
    // -----------
    ui->gbModel->setTitle(tModel);
    ui->gbProject->setTitle(tProject);
    ui->gbSimulation->setTitle(tSimulation);

    // Labels
    // ------
    ui->lbModel->setText(tModelFile);
    ui->lbProject->setText(tProjectName);

    ui->lbVersion->setStyleSheet(clBlue);
    ui->lbInputs->setStyleSheet(clBlue);
    ui->lbParameters->setStyleSheet(clBlue);
    ui->lbOutputs->setStyleSheet(clBlue);
    ui->lbStatus->setStyleSheet(clBlue);
    ui->lbME->setStyleSheet(clBlue);
    ui->lbCo->setStyleSheet(clBlue);
    ui->lbExpData->setStyleSheet(clBlue);
    ui->lbIntegrator->setStyleSheet(clBlue);
    ui->lbSimInt->setStyleSheet(clBlue);

    ui->lbiVersion->setText(tiVersion);
    ui->lbiInputs->setText(tiInputs);
    ui->lbiParameters->setText(tiParameters);
    ui->lbiOutputs->setText(tiOutputs);
    ui->lbiStatus->setText(tiStatus);
    ui->lbiME->setText(tiME);
    ui->lbiCo->setText(tiCO);
    ui->lbiExpData->setText(tDefExp);
    ui->lbiSimInt->setText(tSimInt);
    ui->lbiIntegrator->setText(tIntegrator2);

    // Buttons
    // -------
    ui->bExp->setText(tExperiment);
    ui->bExp->setIcon(icExperiment);
    ui->bExp->setToolTip(sEmpty);
    ui->bExp->setStatusTip(sEmpty);

    ui->bIntegrator->setText(tIntegrator);
    ui->bIntegrator->setIcon(icIntegrator);
    ui->bIntegrator->setToolTip(sEmpty);
    ui->bIntegrator->setStatusTip(sEmpty);

    ui->bParameters->setText(tParameters);
    ui->bParameters->setIcon(icParameters);
    ui->bParameters->setToolTip(sEmpty);
    ui->bParameters->setStatusTip(sEmpty);

    ui->bModelFile->setText(sEmpty);
    ui->bModelFile->setIcon(icModelFile);
    ui->bModelFile->setToolTip(sEmpty);
    ui->bModelFile->setStatusTip(sbOpenFMU);

    ui->bRefresh->setText(sEmpty);
    ui->bRefresh->setIcon(icRefresh);
    ui->bRefresh->setToolTip(sEmpty);
    ui->bRefresh->setStatusTip(sbRefreshFMU);

    ui->bStructure->setText(tStructure);
    ui->bStructure->setIcon(icStructure);
    ui->bStructure->setToolTip(sEmpty);
    ui->bStructure->setStatusTip(sEmpty);

    ui->bFMU->setText(tFMUInfo);
    ui->bFMU->setIcon(icFMU);
    ui->bFMU->setToolTip(sEmpty);
    ui->bFMU->setStatusTip(sEmpty);

    ui->bOutputs->setText(tOutputs);
    ui->bOutputs->setIcon(icOutputs);
    ui->bOutputs->setToolTip(sEmpty);
    ui->bOutputs->setStatusTip(sEmpty);

    ui->bSimulate->setIcon(icSimulate);
    ui->bStop->setIcon(icStop);
    ui->bResults->setIcon(icResults);

    ui->tabLog->setTabIcon(0,icMessages);
    ui->tabLog->setTabIcon(1,icLog);
    ui->tabLog->setTabIcon(2,icLog);

    ui->bSimLogOpen->setIcon(icDocPreview);
    ui->bSimLogClear->setIcon(icNewDocument);
    ui->bOptErrorOpen->setIcon(icDocPreview);
    ui->bOptErrorClear->setIcon(icNewDocument);
    ui->bOptOutOpen->setIcon(icDocPreview);
    ui->bOptOutClear->setIcon(icNewDocument);

    ui->tabOptLog->setTabIcon(0,icLogOut);
    ui->tabOptLog->setTabIcon(1,icCalibrate);

    ui->bEstAlgorithm->setIcon(icCode);
    ui->bEstCalibrate->setIcon(icCalibrate);
    ui->bEstConstraints->setIcon(icConstraints);
    ui->bEstObjFunction->setIcon(icObjective);
    ui->bEstParameters->setIcon(icParameters);
    ui->bEstResults->setIcon(icResults);
    ui->bEstStop->setIcon(icStop);

    ui->bInputEstAlgorithm->setIcon(icCode);
    ui->bInputEstCalibrate->setIcon(icCalibrate);
    ui->bInputEstConstraints->setIcon(icConstraints);
    ui->bInputEstObjective->setIcon(icObjective);
    ui->bInputEstInputs->setIcon(icInputs);
    ui->bInputEstResults->setIcon(icResults);
    ui->bInputEstStop->setIcon(icStop);

    // Tab bar
    // -------
    ui->tabWidget->tabBar()->hide();

    // Log tree
    // --------
    ui->treeLog->setIconSize(QSize(LOG_ICON_SIZE,LOG_ICON_SIZE));

    // Toolbar new project trick
    // ---------------------------------------------------------------
    menu = new QMenu();
    toolButton = new QToolButton();

    menu->addAction(ui->actionSimulation);
    menu->addAction(ui->actionParameter_Calibration);
    menu->addAction(ui->actionInput_estimation);
#ifdef DEBUG
    ui->actionInput_estimation->setVisible(true);
#else
    ui->actionInput_estimation->setVisible(false);
#endif

    toolButton->setIcon(ui->menu_New_Project->icon());
    toolButton->setText(tNewProject);
    toolButton->setToolTip(sEmpty);
    toolButton->setMenu(menu);
    toolButton->setPopupMode(QToolButton::InstantPopup);
    toolButton->setToolButtonStyle(ui->mainToolBar->toolButtonStyle());
    ui->mainToolBar->insertWidget(ui->action_Load_project,toolButton);
    // ---------------------------------------------------------------

}

bool QFMIOptMain::isNotExecuting()
{
    return (thrSim==NULL && thread==NULL);
}

bool QFMIOptMain::isExecuting()
{
    return !isNotExecuting();
}

bool QFMIOptMain::isModelLoaded()
{
    return prj->getModel().valid == FMU_VALID;
}

bool QFMIOptMain::hasProjectChanged()
{
    return prj->getChanged();
}

void QFMIOptMain::setState()
{
    // Enabled when not performing a task
    toolButton->setEnabled(isNotExecuting());
    menu->setEnabled(isNotExecuting());
    ui->menuFile->setEnabled(isNotExecuting());
    ui->menu_New_Project->setEnabled(isNotExecuting());
    ui->actionExit->setEnabled(isNotExecuting());
    ui->actionParameter_Calibration->setEnabled(isNotExecuting());
    ui->action_Load_project->setEnabled(isNotExecuting());
    ui->bModelFile->setEnabled(isNotExecuting());
    ui->bIntegrator->setEnabled(isNotExecuting());
    ui->actionSave_project_as->setEnabled(isNotExecuting());

    // Model loaded, not executing, project with changes
    ui->actionSave_project->setEnabled(hasProjectChanged() && isNotExecuting());

    // Not executing and file selected
    ui->bRefresh->setEnabled(prj->getModel().getFMUfileName()!=sEmpty && isNotExecuting());

    // When model valid and not executing
    ui->bParameters->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bExp->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bOutputs->setEnabled(isModelLoaded() && isNotExecuting());
    ui->actionSimulate->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bStructure->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bFMU->setEnabled(isModelLoaded() && isNotExecuting());

    // Simulation
    ui->actionSimulate->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bSimulate->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bStop->setEnabled(isModelLoaded() && isExecuting());
    //ui->bResults->setEnabled(prj->getExp().simStatus == SIM_FMU_VALID || prj->getExp().simStatus == SIM_FMU_STOP ||
    //                         prj->getExp().simStatus == SIM_FMU_SIMULATION_ERROR);
    ui->bResults->setEnabled(isModelLoaded() && isNotExecuting() && !prj->getExp().time_output.isEmpty() &&
                                                                    !prj->getExp().values_output.isEmpty());

    // Logging
    ui->bOptErrorClear->setEnabled(!ui->textOptError->toPlainText().isEmpty());
    ui->bOptErrorOpen->setEnabled(!ui->textOptError->toPlainText().isEmpty());
    ui->bOptOutClear->setEnabled(!ui->textOptOut->toPlainText().isEmpty());
    ui->bOptOutOpen->setEnabled(!ui->textOptOut->toPlainText().isEmpty());
    ui->bSimLogClear->setEnabled(!ui->textSimLog->toPlainText().isEmpty());
    ui->bSimLogOpen ->setEnabled(!ui->textSimLog->toPlainText().isEmpty());

    // Parameter estimation
    ui->bEstParameters->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bEstAlgorithm->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bEstObjFunction->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bEstCalibrate->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bEstConstraints->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bEstStop->setEnabled(isModelLoaded() && isExecuting());
    ui->bEstResults->setEnabled(isModelLoaded() && isNotExecuting() && !prj->getPrb().getRes_par().isEmpty()
                                                                    && !prj->getPrb().getRes_obj().isEmpty());

    // Input estimation
    ui->bInputEstInputs->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bInputEstAlgorithm->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bInputEstObjective->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bInputEstCalibrate->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bInputEstConstraints->setEnabled(isModelLoaded() && isNotExecuting());
    ui->bInputEstStop->setEnabled(isModelLoaded() && isExecuting());
    ui->bInputEstResults->setEnabled(isModelLoaded() && isNotExecuting() && !prj->getExp().time_output.isEmpty() &&
                                                                            !prj->getExp().values_output.isEmpty());

    // Window title
    setWindowTitlePrj(prj);
}

void QFMIOptMain::on_bParameters_clicked()
{
    Parameters *par = new Parameters();

    par->populateTree(*prj);
    if (par->exec() == QDialog::Accepted)
    {
        opt_model m = prj->getModel();

        m.p_name_new.clear();
        m.p_value_new.clear();
        for(int i=0;i<par->changed.count();i++)
        {
            if (par->changed[i]==true)
            {
                m.p_name_new.append(m.params[i].name);
                m.p_value_new.append(par->new_value[i]);
            }
        }
        prj->setModel(m);
        setState();
    }

    delete par;
}

void QFMIOptMain::sendToLog(QString log, unsigned type, QString time)
{
    QTreeWidgetItem *item = new QTreeWidgetItem;

    item->setIcon(0,type == LOG_ERROR ? QIcon(iError2) :
                   type == LOG_INFO  ? QIcon(iInfo2)  : QIcon(iWarning2));

    item->setToolTip(0,type == LOG_ERROR ? sbError :
                       type == LOG_INFO  ? sbInfo  : sbWarning);
    item->setData(1,Qt::UserRole,type);
    item->setText(1,time.isEmpty() ? QDate::currentDate().toString() + space + QTime::currentTime().toString() : time);
    item->setText(2,log);

    ui->treeLog->addTopLevelItem(item);
}

void QFMIOptMain::writeToLog(QStringList log, QList<unsigned> type, QStringList time)
{
    if(log.count()>0 && log.count()==type.count() && log.count() == time.count())
    {
        for(int i=0;i<log.count();i++)
            sendToLog(log[i],type[i],time[i]);
    }
}

void QFMIOptMain::readFromLog(QStringList &log, QList<unsigned> &type, QStringList &time)
{
    QTreeWidgetItemIterator it(ui->treeLog);

    log.clear();
    type.clear();
    time.clear();

    while (*it)
    {
        type.append((*it)->data(1,Qt::UserRole).toUInt());
        time.append((*it)->text(1));
        log.append((*it)->text(2));
        ++it;
    }
}

void QFMIOptMain::simThread()
{   
    QApplication::setOverrideCursor(Qt::BusyCursor);

    ui->treeLog->clear();
    ui->textSimLog->clear();
    ui->textOptOut->clear();
    ui->textOptError->clear();
    sendToLog("Starting simulation");

    // Simulation thread
    thrSim = new QThread;
    sim    = new simulation(prj);

    setState();
    sim->moveToThread(thrSim);
    connect(thrSim, SIGNAL(started()),   sim,    SLOT(run()));
    connect(sim,    SIGNAL(end()),       thrSim, SLOT(quit()));
    connect(sim,    SIGNAL(end()),       sim,    SLOT(deleteLater()));
    connect(thrSim, SIGNAL(finished()),  thrSim, SLOT(deleteLater()));

    connect(sim, SIGNAL(simUpdate(double,double,double)),                  this, SLOT(simUpdate(double,double,double)));
    connect(sim, SIGNAL(simError(unsigned, QString)),                      this, SLOT(simError(unsigned, QString)));
    connect(sim, SIGNAL(finished(double, opt_exp, opt_problem, unsigned)), this, SLOT(simFinished(double, opt_exp, opt_problem, unsigned)));
    simOutFile = getSimFileNameLog();
    thrSim->start();
}

void QFMIOptMain::simUpdate(double t, double startTime, double stopTime)
{
    // GUI info
    ui->lbProgress->setText(QString::number(t) +" / "+QString::number(stopTime)+" (s)");
    ui->pbProgress->setValue(100 * ((t-startTime)/(stopTime-startTime)));
    ui->pbProgress->setProperty(PB_TIME.toStdString().c_str(),t);
    updateSimLog();
}

void QFMIOptMain::simError(unsigned simStatus, QString msg)
{
    Q_UNUSED(simStatus);

    sendToLog(msg,LOG_ERROR);
    updateSimLog();
    QApplication::restoreOverrideCursor();
}

void QFMIOptMain::simFinished(double t, opt_exp exp, opt_problem prb, unsigned status)
{
    Q_UNUSED(prb);
    Q_UNUSED(status);

    // Write project results
    prj->setExp(exp);

    unsigned simStatus = prj->getExp().simStatus;
    if (simStatus == SIM_FMU_VALID)     sendToLog("Simulation finished");
    else if (simStatus == SIM_FMU_STOP) sendToLog("Simulation stopped",LOG_WARNING);
    sendToLog("Execution time: " + msToTime(t));
    updateSimLog();
    // Write project result information
    writeProjectResultInfo();
    // Wait thread to finish
    while (thrSim->isRunning())
        QApplication::processEvents();
    thrSim = NULL;
    setState();
    QApplication::restoreOverrideCursor();
}

void QFMIOptMain::writeProjectResultInfo()
{
    QStringList     log;
    QList<unsigned> type;
    QStringList     time;

    readFromLog(log,type,time);

    prj->setPercentage(ui->pbProgress->value());
    prj->setPerTime(ui->pbProgress->property(PB_TIME.toStdString().c_str()).toDouble());
    prj->setLmesg(log);
    prj->setLtype(type);
    prj->setLtime(time);
}

void QFMIOptMain::on_actionSimulate_triggered()
{
    simThread();
}

void QFMIOptMain::on_action_Load_project_triggered()
{
    if (!prj->getChanged() || saveChanges())
    {
        QString path = prj->getFilename();
        QString file = QFileDialog::getOpenFileName(this, mProjectFileOpen, path, QPFFilter);

        load_project(file);
    }
}

void QFMIOptMain::open_recent_project()
{
    if (!prj->getChanged() || saveChanges())
    {
        QString file = ((QAction *)sender())->data().toString();

        load_project(file);
    }
}

void QFMIOptMain::load_project(QString file)
{
    if (!file.isEmpty())
    {
        prj->deletePrbMethodArgs();
        delete prj;
        prj = new opt_project(ptNoType);
        prj->load_from_file(file);
        setInterfaceDefault();
        infoFMU(prj->getModel().getFMUfileName());
        setInterfaceFormProject();
        setCurrentFile(file);
    }
}

void QFMIOptMain::setSimInfoProject()
{
    ui->lbSimInt->setText("("+QString::number(prj->getExp().startTime,NUM_FORMAT,NUM_DEC) +","+ QString::number(prj->getExp().stopTime,NUM_FORMAT,NUM_DEC)+")");
    ui->lbIntegrator->setText(integratorToText(prj->getSim().integrator));
    ui->lbProgress->setText(QString::number(prj->getPerTime()) + " / " + QString::number(prj->getExp().stopTime) + " (s)");
    ui->pbProgress->setValue(prj->getPercentage());
    ui->pbProgress->setProperty(PB_TIME.toStdString().c_str(),prj->getPerTime());
}

void QFMIOptMain::setWindowTitlePrj(opt_project *prj)
{
    QString title = appName + space + appVer;

    if (prj != NULL && !prj->getFilename().isEmpty())
    {
        QFontMetrics fm(font());

        title = title + space + script + space + fm.elidedText(prj->getFilename(),Qt::ElideMiddle,width());
        if (prj->getChanged()) title = title + space + asterisk;
    }
    if (title != windowTitle()) setWindowTitle(title);
}

void QFMIOptMain::setInterfaceFormProject()
{
    QFontMetrics fm(ui->leModelFile->font());
    QString      FMUfile = fileAbsoluteFromProject(prj,prj->getModel().getFMUfileName());

    // Set data in the interface
    ui->leProjectName->setText(prj->getName());
    ui->leProjectName->setStatusTip(prj->getFilename());
    ui->leModelFile->setText(fm.elidedText(FMUfile,Qt::ElideMiddle,ui->leModelFile->width()-WIDTH_EPS));
    ui->leModelFile->setStatusTip(FMUfile);
    ui->treeLog->clear();
    ui->textSimLog->clear();
    ui->textOptOut->clear();
    ui->textOptError->clear();
    setSimInfoProject();
    writeToLog(prj->getLmesg(),prj->getLtype(),prj->getLtime());

    // Project has not been changed
    prj->setChanged(false);

    // Set state of the interface
    setState();
}

bool QFMIOptMain::saveChanges()
{
    QMessageBox *msgBox = new QMessageBox(this);
    int response;

    msgBox->setText(mSaveProject);
    msgBox->setStandardButtons(QMessageBox::Save | QMessageBox::No |
                               QMessageBox::Cancel);
    msgBox->setDefaultButton(QMessageBox::Save);
    response = msgBox->exec();
    delete msgBox;

    if (response == QMessageBox::Save)
    {
        on_actionSave_project_triggered();
        if (!prj->getChanged())
            return true;
        else
            return false;
    }
    else if (response == QMessageBox::No)
    {
        return true;
    }
    else
    {
        return false;
    }
    return response;
}

void QFMIOptMain::on_actionSave_project_triggered()
{
    if (!prj->getFilename().isEmpty())
    {
        prj->save_to_file();
        ui->statusBar->showMessage(mProjectSaved,sbTime);
        ui->leProjectName->setStatusTip(prj->getFilename());
    }
    else
        on_actionSave_project_as_triggered();
    setWindowTitlePrj(prj);
}

void QFMIOptMain::on_actionSave_project_as_triggered()
{
    QFileDialog dlg(this, mProjectFileSave, sEmpty, QPFFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
        opt_model mo      = prj->getModel();
        opt_exp   ex      = prj->getExp();
        QString   FMUfile = fileAbsoluteFromProject(prj,mo.getFMUfileName());
        QString   EXPfile = fileAbsoluteFromProject(prj,ex.fileName);

        prj->setFilename(dlg.selectedFiles()[0]);
        if (!mo.getFMUfileName().isEmpty())
        {
            mo.setFMUfileName(fileRelativeToProject(prj,FMUfile),projectPath(prj));
            prj->setModel(mo);
        }
        if (!ex.fileName.isEmpty())
        {
            ex.fileName = fileRelativeToProject(prj,EXPfile);
            prj->setExp(ex);
        }
        on_actionSave_project_triggered();
        setCurrentFile(dlg.selectedFiles()[0]);
    }
}

QString stringVector(QStringList list)
{
    QString cad = sEmpty;

    if (list.count()>0)
    {
        for (int i=0; i<list.count(); i++)
            cad = cad + list[i] + newLine;
        cad = cad.mid(0,cad.length()-newLine.length());
    }
    return cad;
}

QString varVector(QList<variable> list)
{
    QString cad = sEmpty;

    if (list.count()>0)
    {
        for (int i=0; i<list.count(); i++)
            cad = cad + list[i].name + newLine;
        cad = cad.mid(0,cad.length()-newLine.length());
    }
    return cad;
}

QString stringExpData(opt_model m)
{
    QString cad = sEmpty;

    if (m.expData)
    {
        // Checking for NAN when a!=a
        //cad = (m.expData == m.expData)           ? cad + tStartTime + space + QString::number(m.expStart,NUM_FORMAT,NUM_DEC)     + newLine : cad;
        //cad = (m.expStop == m.expStop)           ? cad + tFinalTime + space + QString::number(m.expStop,NUM_FORMAT,NUM_DEC)      + newLine : cad;
        //cad = (m.expTolerance == m.expTolerance) ? cad + tTolerance + space + QString::number(m.expTolerance,NUM_FORMAT,NUM_DEC) + newLine : cad;
        //cad = (m.expStepSize == m.expStepSize)   ? cad + tStepSize  + space + QString::number(m.expStepSize,NUM_FORMAT,NUM_DEC)  + newLine : cad;
        //cad = cad.mid(0,cad.length()-newLine.length());

        !std::isnan(m.expData)      ? cad + tStartTime + space + QString::number(m.expStart,NUM_FORMAT,NUM_DEC)     + newLine : cad;
        !std::isnan(m.expStop)      ? cad + tFinalTime + space + QString::number(m.expStop,NUM_FORMAT,NUM_DEC)      + newLine : cad;
        !std::isnan(m.expTolerance) ? cad + tTolerance + space + QString::number(m.expTolerance,NUM_FORMAT,NUM_DEC) + newLine : cad;
        !std::isnan(m.expStepSize)  ? cad + tStepSize  + space + QString::number(m.expStepSize,NUM_FORMAT,NUM_DEC)  + newLine : cad;
        cad = cad.mid(0,cad.length()-newLine.length());
    }
    return cad;
}

void QFMIOptMain::infoFMU(QString filename)
{
    opt_model m = prj->getModel();

    m.setFMUfileName(filename,projectPath(prj));
    prj->setModel(m);
    m = prj->getModel();
    if (m.valid == FMU_VALID)
    {
        ui->lbStatus->setText(tOk);
        ui->lbStatus->setStyleSheet(clGreen);
        ui->lbME->setText(m.modelExchanged ? tYes : tNo);
        ui->lbCo->setText(m.cosimulation ? tYes : tNo);
        ui->lbVersion->setText((m.fmiVer == FMI_2_0) ? tFMI20 : tFMI10);
        //ui->lbDate->setText(m.date.toString(dfSCREEN1));
        //ui->lbTool->setText(m.vendor);
        ui->lbInputs->setText(QString::number(m.inputs.count(),NUM_FORMAT,NUM_DEC));
        ui->lbParameters->setText(QString::number(m.params.count(),NUM_FORMAT,NUM_DEC));
        ui->lbOutputs->setText(QString::number(m.outputs.count(),NUM_FORMAT,NUM_DEC));
        ui->lbExpData->setText(m.expData ? tYes : tNo);

        QFont fInputs = ui->lbInputs->font();
        fInputs.setUnderline(m.inputs.count()>0);
        ui->lbInputs->setFont(fInputs);

        QFont fParameters = ui->lbParameters->font();
        fParameters.setUnderline(m.params.count()>0);
        ui->lbParameters->setFont(fParameters);

        QFont fOutput = ui->lbOutputs->font();
        fOutput.setUnderline(m.outputs.count()>0);
        ui->lbOutputs->setFont(fOutput);

        QFont fExpData = ui->lbExpData->font();
        fExpData.setUnderline(m.expData);
        ui->lbExpData->setFont(fExpData);

        if (m.inputs.count()>0)  ui->lbInputs->setToolTip(varVector(m.inputs));     else ui->lbInputs->setToolTip(sEmpty);
        if (m.outputs.count()>0) ui->lbOutputs->setToolTip(varVector(m.outputs));   else ui->lbOutputs->setToolTip(sEmpty);
        if (m.params.count()>0)  ui->lbParameters->setToolTip(varVector(m.params)); else ui->lbParameters->setToolTip(sEmpty);
        if (m.expData)           ui->lbExpData->setToolTip(stringExpData(m));       else ui->lbExpData->setToolTip(sEmpty);

        if (m.vendor.indexOf(tLicense,0,Qt::CaseInsensitive) >= 0)
            ui->bFMU->setStyleSheet(clRed);
        else
            ui->bFMU->setStyleSheet(sEmpty);
    }
    else
    {
        ui->lbStatus->setText(tError);
        ui->lbStatus->setStatusTip(m.valid == FMU_XML_ERROR ? tErrorXMlFile : tErrorZipFile);
        ui->lbStatus->setStyleSheet(clRed);
        ui->lbME->setText(sEmpty);
        ui->lbCo->setText(sEmpty);
        ui->lbVersion->setText(sEmpty);
        ui->lbInputs->setText(sEmpty);
        ui->lbParameters->setText(sEmpty);
        ui->lbOutputs->setText(sEmpty);
        ui->lbExpData->setText(sEmpty);

        QFont fInputs = ui->lbInputs->font();
        fInputs.setUnderline(false);
        ui->lbInputs->setFont(fInputs);

        QFont fParameters = ui->lbParameters->font();
        fParameters.setUnderline(false);
        ui->lbParameters->setFont(fParameters);

        QFont fOutput = ui->lbOutputs->font();
        fOutput.setUnderline(false);
        ui->lbOutputs->setFont(fOutput);

        QFont fExpData = ui->lbExpData->font();
        fExpData.setUnderline(false);
        ui->lbExpData->setFont(fExpData);

        ui->lbInputs->setToolTip(sEmpty);
        ui->lbOutputs->setToolTip(sEmpty);
        ui->lbParameters->setToolTip(sEmpty);
        ui->lbExpData->setToolTip(sEmpty);
    }
    setState();
}

void QFMIOptMain::on_leProjectName_textChanged(const QString &arg1)
{
    prj->setName(arg1);
    setState();
}

void QFMIOptMain::closeEvent(QCloseEvent * event)
{
    event->ignore();
    if (!prj->getChanged())
    {
        if (QMessageBox::Yes == QMessageBox::question(this, mCloseConfirmation,
                              mExit,
                              QMessageBox::Yes|QMessageBox::No))
        {
            event->accept();
        }
    }
    else
        if (saveChanges())
            event->accept();
        else
            event->ignore();
}

void QFMIOptMain::on_bRefresh_clicked()
{
    infoFMU(prj->getModel().getFMUfileName());
    setState();
}

void QFMIOptMain::on_bIntegrator_clicked()
{
    integrator *inte = new integrator();

    inte->setIntInfo(prj->getSim());
    if (inte->exec() == QDialog::Accepted)
    {
        prj->setSim(inte->getIntInfo());
        setSimInfoProject();
        setState();
    }
    delete inte;
}

void QFMIOptMain::on_bExp_clicked()
{
    experiment *exp = new experiment();

    exp->setModelExp(prj);
    if (exp->exec())
    {
        prj->setExp(exp->getExp());
        setSimInfoProject();
        setState();
    }
    delete exp;
}

void QFMIOptMain::on_bSimulate_clicked()
{
    simThread();
}

void QFMIOptMain::on_bStop_clicked()
{
    if (sim!=NULL) sim->stopSim();
}

void QFMIOptMain::on_bStructure_clicked()
{
    modelStructure *ms = new modelStructure;

    QApplication::setOverrideCursor(Qt::BusyCursor);
    ms->populateTree(*prj);
    QApplication::restoreOverrideCursor();
    ms->exec();
    ms->saveGeoInfo();
    delete ms;
}

void QFMIOptMain::newProject(int projectType)
{
    if(prj == NULL || !prj->getChanged() || saveChanges())
    {
        // Delete previous project
        if (prj!=NULL) prj->deletePrbMethodArgs();
        delete prj;
        // New simulation project
        prj = new opt_project(projectType);
        // Clear interface
        setInterfaceDefault();
    }
}

void QFMIOptMain::on_actionSimulation_triggered()
{
    newProject(ptSimulation);
    setState();
}

void QFMIOptMain::on_actionParameter_Calibration_triggered()
{
    newProject(ptEstimation);
    setState();
}

void QFMIOptMain::on_bOutputs_clicked()
{
    outputs *out = new outputs();

    out->setModelExp(*prj);
    if (out->exec())
    {
        prj->setExp(out->getExp());
        setState();
    }
    delete out;
}

void QFMIOptMain::on_bResults_clicked()
{
    QList<variable>      vars;
    QList<QList<double>> values;
    plotgraphs          *plot = new plotgraphs();

    // Add time variable (indepedent variable) for plotting
    vars.append(varTimeDef());
    values.append(prj->getExp().time_output);

    // Add inputs
    if (prj->getModel().inputs.count()>0)
    {
        vars.append(prj->getModel().inputs);
        values.append(prj->getExp().values_input);
    }

    // Add outputs
    if (prj->getModel().outputs.count()>0)
    {
        vars.append(prj->getModel().outputs);
        values.append(prj->getExp().values_output);
    }

    // Load data in plotting tool
    plot->loadData(vars,values);
    plot->setAttribute(Qt::WA_DeleteOnClose);
    plot->setWindowModality(Qt::ApplicationModal);
    plot->show();
    // WARNING: this automatically close the window
    // TODO:    find a solution to delete plot and avoid memory leaks
    // delete plot
}

void QFMIOptMain::on_bEstAlgorithm_clicked()
{
    parEstAlgorithm *pea = new parEstAlgorithm();

    pea->setMethod(prj->getPrb().getMethod(),prj->getType());
    if(pea->exec())
    {
        opt_problem p = prj->getPrb();

        deleteArgs(p.getMethod().getArgs());
        p.setMethod(pea->getMethod());
        prj->setPrb(p);
        setState();
    }
    else
        deleteArgs(pea->getMethod().getArgs());

    delete pea;
}

void QFMIOptMain::on_bEstParameters_clicked()
{
    parestparameters *pep = new parestparameters();

    pep->setParams(*prj);
    if(pep->exec())
    {
        opt_problem prb = prj->getPrb();
        prb.setParameters(pep->getPar());
        prj->setPrb(prb);
        setState();
    }
    delete pep;
}

void QFMIOptMain::on_bEstObjFunction_clicked()
{
    parestobjective *pef = new parestobjective();

    pef->setProject(*prj);
    if(pef->exec())
    {
        opt_problem prb = prj->getPrb();
        prb.setObjFncs(pef->getObjFncs());
        prj->setPrb(prb);
        setState();
    }
    delete pef;
}

void QFMIOptMain::on_bFMU_clicked()
{
    FMUinfo *fmu = new FMUinfo;

    fmu->populateTree(*prj);
    fmu->exec();
    fmu->saveGeoInfo();
}

bool QFMIOptMain::checkParEstCalibrate()
{
    return true;
}

bool QFMIOptMain::checkInputEstimation()
{
    return true;
}

void QFMIOptMain::parEstCalibrateThread()
{
    if (checkParEstCalibrate())
    {
        QApplication::setOverrideCursor(Qt::BusyCursor);

        ui->treeLog->clear();
        ui->textSimLog->clear();
        ui->textOptOut->clear();
        ui->textOptError->clear();
        sendToLog("Starting parameter estimation");

        // Parameter estimation thread
        thread = new QThread;
        parEst = new parameterEstimation(prj);

        setState();
        parEst->moveToThread(thread);
        connect(thread, SIGNAL(started()),   parEst, SLOT(run()));
        connect(parEst, SIGNAL(end()),       thread, SLOT(quit()));
        connect(parEst, SIGNAL(end()),       parEst, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()),  thread, SLOT(deleteLater()));

        connect(parEst, SIGNAL(parEstUpdate(double,double,double)),            this, SLOT(estimationUpdate(double,double,double)));
        connect(parEst, SIGNAL(parEstError(unsigned, QString)),                this, SLOT(estimationError(unsigned, QString)));
        connect(parEst, SIGNAL(finished(double,opt_exp,opt_problem,unsigned)), this, SLOT(estimationFinished(double,opt_exp,opt_problem,unsigned)));
        simOutFile   = getSimFileNameLog();
        optOutFile   = parEst->getOutFile();
        optErrorFile = parEst->getErrorFile();

        thread->start();
    }
}

void QFMIOptMain::inputEstimationThread()
{
    if (checkInputEstimation())
    {
        QApplication::setOverrideCursor(Qt::BusyCursor);

        ui->treeLog->clear();
        ui->textSimLog->clear();
        ui->textOptOut->clear();
        ui->textOptError->clear();
        sendToLog("Starting input estimation");

        // Input estimation thread
        thread   = new QThread;
        inputEst = new inputEstimation(prj);

        setState();
        inputEst->moveToThread(thread);
        connect(thread,   SIGNAL(started()),   inputEst, SLOT(run()));
        connect(inputEst, SIGNAL(end()),       thread,   SLOT(quit()));
        connect(inputEst, SIGNAL(end()),       inputEst, SLOT(deleteLater()));
        connect(thread,   SIGNAL(finished()),  thread,   SLOT(deleteLater()));

        connect(inputEst, SIGNAL(inputEstUpdate(double,double,double)),          this, SLOT(estimationUpdate(double,double,double)),Qt::DirectConnection);
        connect(inputEst, SIGNAL(inputEstError(unsigned, QString)),              this, SLOT(estimationError(unsigned, QString)),Qt::DirectConnection);
        connect(inputEst, SIGNAL(finished(double,opt_exp,opt_problem,unsigned)), this, SLOT(estimationFinished(double,opt_exp,opt_problem,unsigned)),Qt::DirectConnection);
        simOutFile   = getSimFileNameLog();
        optOutFile   = inputEst->getOutFile();
        optErrorFile = inputEst->getErrorFile();

        // WARNING: configuration in GUI
        // consider it in parameter estimation as well
        inputEst->setDakota_log_level(DAKOTA_QUIET);

        thread->start();
    }
}

void QFMIOptMain::estimationUpdate(double t, double tstart, double tstop)
{
    Q_UNUSED(t);
    Q_UNUSED(tstart);
    Q_UNUSED(tstop);

    updateSimLog();
    updateOptLog();
}

QString readLinesFromFile(QString filename, unsigned num)
{
    QFile file(filename);

    if(file.open(QIODevice::ReadOnly))
    {
        QString line;
        qint64  pos = file.size() - num;
        char    c;

        file.seek(pos);
        do{
            pos--;
            file.seek(pos);
            file.getChar(&c);
        }while(c!='\n' && file.pos()>0);
        line = file.read(num);
        file.close();
        return line;
    }
    return sEmpty;
}


void updateLog(QString filename, QTextEdit *text)
{
    unsigned num = NUM_BYTES_LOG;

    if (!filename.isEmpty())
    {
        bool oldState = text->blockSignals(true);
        text->setText(readLinesFromFile(filename,num));
        text->verticalScrollBar()->setValue(text->verticalScrollBar()->maximum());
        text->blockSignals(oldState);
    }
}

void QFMIOptMain::updateSimLog()
{
    updateLog(simOutFile,ui->textSimLog);
    setState();
    QApplication::processEvents();
}

void QFMIOptMain::updateOptLog()
{
    updateLog(optOutFile,ui->textOptOut);
    updateLog(optErrorFile,ui->textOptError);
    setState();
    QApplication::processEvents();
}

void QFMIOptMain::estimationError(unsigned simStatus, QString msg)
{
    Q_UNUSED(simStatus);

    sendToLog(msg,LOG_ERROR);

    updateSimLog();
    updateOptLog();
    setState();
    QApplication::restoreOverrideCursor();
}

void QFMIOptMain::estimationFinished(double t, opt_exp exp, opt_problem prb, unsigned status)
{
    prj->setExp(exp);
    prj->setPrb(prb);

    if (status == DAKOTA_OK)            sendToLog(prj->getType() == ptInputEst ?
                                                      "Input estimation finished" :
                                                      "Parameter estimation finished");
    else if (status == DAKOTA_STOPPED)  sendToLog(prj->getType() == ptInputEst ?
                                                      "Input estimation stopped" :
                                                      "Parameter estimation stopped",LOG_WARNING);
    sendToLog("Execution time: " + msToTime(t));
    // WARNING: Wait thread to finish. Slow when executing several times
    while (thread->isRunning())
        QApplication::processEvents();
    thread = NULL;

    updateSimLog();
    updateOptLog();
    // Write project result information
    writeProjectResultInfo();
    // Set state
    setState();
    QApplication::restoreOverrideCursor();
}

void clearLogText(QTextEdit *text)
{
    text->clear();
}

void QFMIOptMain::on_bOptOutClear_clicked()
{
    clearLogText(ui->textOptOut);
    setState();
}

void QFMIOptMain::on_bOptErrorClear_clicked()
{
    clearLogText(ui->textOptError);
    setState();
}

void QFMIOptMain::on_bSimLogClear_clicked()
{
    clearLogText(ui->textSimLog);
    setState();
}

void QFMIOptMain::on_bOptOutOpen_clicked()
{
    openFileInEditor(optOutFile,textEditor);
}

void QFMIOptMain::on_bOptErrorOpen_clicked()
{
    openFileInEditor(optErrorFile,textEditor);
}

void QFMIOptMain::on_bEstCalibrate_clicked()
{
    parEstCalibrateThread();
}

void QFMIOptMain::on_bEstStop_clicked()
{
    if (parEst != 0) parEst->requestStop();
}

void QFMIOptMain::on_bEstResults_clicked()
{
    parestresults *per = new parestresults();

    per->setData(prj->getPrb().getRes_par(),
                 prj->getPrb().getRes_obj(),
                 prj->getPrb().getRes_con());
    per->exec();

    delete per;
}

void QFMIOptMain::on_bEstConstraints_clicked()
{
    parestconstraints *pec = new parestconstraints();
    opt_problem        prb = prj->getPrb();

    pec->setData(prb.getParameters(),prj->getModel().outputs,prb.getLinear_eq(),prb.getLinear_ie(),prb.getNon_linear_eq(),prb.getNon_linear_ie(),prj->getType());
    if (pec->exec())
    {
        prb.setLinear_eq(pec->getLinear_eq());
        prb.setLinear_ie(pec->getLinear_ie());
        prb.setNon_linear_eq(pec->getNon_linear_eq());
        prb.setNon_linear_ie(pec->getNon_linear_ie());
        prj->setPrb(prb);
        setState();
    }
    delete pec;
}

void QFMIOptMain::on_actionOptions_triggered()
{
    options *opt = new options();

    if (opt->exec())
    {
        textEditor = readOption(optTextEditor,defTextEditor);
        ui->mainToolBar->setHidden(readOption(optToolBar,defToolBar));
    }
    delete opt;
}

void QFMIOptMain::on_actionInput_estimation_triggered()
{
    newProject(ptInputEst);
    setState();
}

void QFMIOptMain::on_bInputEstAlgorithm_clicked()
{
    ui->bEstAlgorithm->click();
}

void QFMIOptMain::on_bInputEstObjective_clicked()
{
    ui->bEstObjFunction->click();
}

void QFMIOptMain::on_bInputEstConstraints_clicked()
{
    ui->bEstConstraints->click();
}

void QFMIOptMain::on_bInputEstInputs_clicked()
{
    ui->bEstParameters->click();
}

void QFMIOptMain::on_bInputEstCalibrate_clicked()
{
    inputEstimationThread();
}

void QFMIOptMain::on_bInputEstStop_clicked()
{
    if (inputEst != 0) inputEst->requestStop();
}

void QFMIOptMain::on_bInputEstResults_clicked()
{
    ui->bResults->click();
}
