#ifndef QFMIOPTMAIN_H
#define QFMIOPTMAIN_H

#include <QMainWindow>
#include <QCloseEvent>

#include "opt_project.h"
#include "simulation.h"
#include "parameterestimation.h"
#include "inputestimation.h"
#include "plotgraphs.h"

namespace Ui {
class QFMIOptMain;
}

class QFMIOptMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit QFMIOptMain(QWidget *parent = 0);
    void setInterfaceConf();
    void setState();
    void simThread();
    bool saveChanges();
    void defaultProject();
    void setInterfaceFormProject();
    void setInterfaceDefault();
    ~QFMIOptMain();
    void closeEvent(QCloseEvent * event);
    void infoFMU(QString filename);
    void setSimInfoProject();
    void sendToLog(QString log, unsigned type = LOG_INFO, QString time = sEmpty);
    void writeToLog(QStringList log, QList<unsigned> type, QStringList time);
    void readFromLog(QStringList &log, QList<unsigned> &type, QStringList &time);
    void newProject(int projectType);
    void setSimOutputs(opt_exp exp);
    void parEstCalibrateThread();
    bool checkParEstCalibrate();
    void writeProjectResultInfo();
    void load_project(QString file);
    bool checkInputEstimation();
    void inputEstimationThread();
    void setWindowTitlePrj(opt_project *prj = NULL);

    opt_project *prj;

private slots:
    void on_actionExit_triggered();
    void on_bModelFile_clicked();
    void on_bParameters_clicked();
    void on_actionSimulate_triggered();
    void on_action_Load_project_triggered();
    void on_actionSave_project_triggered();
    void on_actionSave_project_as_triggered();
    void on_leProjectName_textChanged(const QString &arg1);
    void on_bRefresh_clicked();
    void on_bIntegrator_clicked();
    void on_bExp_clicked();
    void on_bSimulate_clicked();
    void on_bStop_clicked();
    void on_bStructure_clicked();
    void on_actionSimulation_triggered();
    void on_actionParameter_Calibration_triggered();
    void on_bOutputs_clicked();
    void on_bResults_clicked();
    void on_bEstAlgorithm_clicked();
    void on_bEstParameters_clicked();
    // Simulation
    void simUpdate(double t, double start, double stop);
    void simFinished(double t, opt_exp exp, opt_problem prb, unsigned status);
    void simError(unsigned simStatus,QString msg);
    void updateSimLog();
    // Parameter or input estimation
    void estimationUpdate(double t, double tstart, double tstop);
    void estimationFinished(double t, opt_exp exp, opt_problem opt, unsigned status);
    void estimationError(unsigned simStatus, QString msg);
    void updateOptLog();
    // --------------------
    void on_bEstObjFunction_clicked();
    void on_bFMU_clicked();
    void on_bOptOutClear_clicked();
    void on_bOptErrorClear_clicked();
    void on_bSimLogClear_clicked();
    void on_bOptOutOpen_clicked();
    void on_bOptErrorOpen_clicked();
    void on_bEstCalibrate_clicked();
    void on_bEstStop_clicked();
    void on_bEstResults_clicked();
    void on_bEstConstraints_clicked();
    void open_recent_project();
    void on_actionOptions_triggered();
    void toolBarVisibility(bool visible);
    void updateRecentFileActions();
    void setCurrentFile(const QString &fileName);

    void on_actionInput_estimation_triggered();

    void on_bInputEstAlgorithm_clicked();

    void on_bInputEstObjective_clicked();

    void on_bInputEstConstraints_clicked();

    void on_bInputEstInputs_clicked();

    void on_bInputEstCalibrate_clicked();

    void on_bInputEstStop_clicked();

    void on_bInputEstResults_clicked();

private:
    Ui::QFMIOptMain     *ui;
    simulation          *sim;
    QThread             *thrSim;
    QThread             *thread;
    parameterEstimation *parEst;
    inputEstimation     *inputEst;
    plotgraphs           plot;
    QString              simOutFile;
    QString              optOutFile;
    QString              optErrorFile;
    QString              textEditor;

    enum {MaxRecentFiles=5};
    QAction *recentFileActs[MaxRecentFiles];

    bool isNotExecuting();
    bool isExecuting();
    bool isModelLoaded();
    bool hasProjectChanged();
};

#endif // QFMIOPTMAIN_H

