#include "of_process.h"

#include <iostream>
#include <mpi.h>
#include "labels.h"

using namespace std;

of_process::of_process(opt_project *project, QString startMessage, QString finishMessage)
{
    // Store project
    prj = project;

    // Store start and finish messages
    sStartMessage  = startMessage;
    sFinishMessage = finishMessage;

    // Create a simulation thread
    thr = new QThread;

    // Thread signal/slog
    connect(thr, SIGNAL(finished()), thr, SLOT(deleteLater()));

    // Set not error
    err = false;
}

of_process::~of_process()
{
    delete thr;
}

bool of_process::run()
{
    QEventLoop loop;

    // Connect loop to thread finished
    connect(thr, SIGNAL(finished()), &loop, SLOT(quit()));

    // Console info
    cout << endl;
    cout << sStartMessage.toStdString() << endl;
    cout << endl;

    // Log message
    sendToLog(log,ltype,ltime,sStartMessage);

    // Start thread
    thr->start();

    // Wait for finish thread event
    loop.exec();

    // Return simulation status
    return err;
}

void of_process::proError(unsigned simStatus, QString msg)
{
    Q_UNUSED(simStatus);

    // Set simulation error
    err = true;

    // Console mesagge
    cout << endl;
    cout << msg.toStdString() << endl;
    cout << endl;

    // Log message
    sendToLog(log,ltype,ltime,msg,LOG_ERROR);
}

void of_process::proFinished(double t, opt_exp exp, opt_problem prb, unsigned status)
{
    Q_UNUSED(status);

    // Simulation finished and execution time string
    QString sExecutionTime = m_executionTime + t_space + msToTime(t);

    // Console messages
    cout << endl;
    cout << sFinishMessage.toStdString() << endl;
    cout << sExecutionTime.toStdString() << endl;
    cout << m_resultSaveIn.toStdString() << t_space.toStdString() << prj->getFilename().toStdString() << endl;
    cout << endl;

    // Send to log
    sendToLog(log,ltype,ltime,sFinishMessage);
    sendToLog(log,ltype,ltime,sExecutionTime);

    // Get rank
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Only root rank
    if (rank == 0)
    {

        // Write project results
        prj->setExp(exp);

        // Write problem
        prj->setPrb(prb);

        // Write project result information
        writeProjectResultInfo();

        // Save project
        prj->save_to_file();
    }
}

void of_process::writeProjectResultInfo()
{
    prj->setLmesg(log);
    prj->setLtype(ltype);
    prj->setLtime(ltime);
}
