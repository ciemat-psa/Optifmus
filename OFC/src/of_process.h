#ifndef OF_PROCESS_H
#define OF_PROCESS_H

#include "opt_project.h"
#include "common.h"

class of_process: public QObject
{
    Q_OBJECT

protected:
    opt_project *prj;
    QThread     *thr;

private:
    bool err;

    QStringList     log;
    QList<unsigned> ltype;
    QStringList     ltime;

    QString sStartMessage;
    QString sFinishMessage;

public:
    of_process(opt_project *project, QString startMessage, QString finishMessage);
    ~of_process();
    bool run();

protected slots:
    virtual void writeProjectResultInfo();
    virtual void proUpdate(double, double, double) = 0;

private slots:
    void proError(unsigned, QString);
    void proFinished(double t, opt_exp exp, opt_problem prb, unsigned status);
};

#endif // OF_PROCESS_H
