#include "of_simulation.h"

#include <iostream>
#include "labels.h"

using namespace std;

of_simulation::of_simulation(opt_project *project)
    :of_process(project,m_startingSimulation,m_simFinished)
{
    // Create simulation class
    sim = new simulation(prj);

    // Connect simulation to thread
    sim->moveToThread(thr);
    connect(thr, SIGNAL(started()), sim, SLOT(run()));
    connect(sim, SIGNAL(end()),     thr, SLOT(quit()));
    connect(sim, SIGNAL(end()),     sim, SLOT(deleteLater()));

    // Set slots for showing information
    connect(sim, SIGNAL(simUpdate(double,double,double)), this, SLOT(proUpdate(double,double,double)));
    connect(sim, SIGNAL(simError(unsigned, QString)),     this, SLOT(proError(unsigned, QString)));
    connect(sim, SIGNAL(finished(double, opt_exp, opt_problem, unsigned)), this, SLOT(proFinished(double, opt_exp, opt_problem, unsigned)));
}

void of_simulation::proUpdate(double t, double startTime, double stopTime)
{
    per  = 100 * ((t-startTime)/(stopTime-startTime));
    time = t;
    QString cadTime = QString::number(t) +" / "+QString::number(stopTime)+" (s)";

    // Console info
    cout << ">>" << t_space.toStdString() << QString::number(per).toStdString() << "% - " << cadTime.toStdString() << endl;
}

void of_simulation::writeProjectResultInfo()
{
    prj->setPercentage(per);
    prj->setPerTime(time);
    of_process::writeProjectResultInfo();
}
