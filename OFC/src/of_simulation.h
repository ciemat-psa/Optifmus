#ifndef OF_SIMULATION_H
#define OF_SIMULATION_H

#include "of_process.h"
#include "opt_project.h"
#include "simulation.h"
#include "common.h"

class of_simulation: public of_process
{
    Q_OBJECT

private:
    simulation  *sim;

    double per;
    double time;

public:
    of_simulation(opt_project *project);

protected:
    void writeProjectResultInfo();

protected slots:
    void proUpdate(double t, double startTime, double stopTime);
};

#endif // OF_SIMULATION_H
