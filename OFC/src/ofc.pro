#--------------------------------------------------#
#                                                  #
# Project created by QtCreator 2017-02-16T21:23:00 #
#                                                  #
#--------------------------------------------------#

QT += core
QT -= gui

CONFIG += c++11

TARGET = ofc
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    ../../common/opt_project.h \
    ../../common/common.h \
    ../../common/formats/libcsv.h \
    ../../common/formats/read_csv.h \
    ../../common/formats/read_matlab4.h \
    ../../common/formats/write_matlab4.h \
    ../../common/opt_problem.h \
    ../../common/dakota/dakota_methods.h \
    ../../common/simulation.h \
    ../../common/parameterestimation.h \
    ../../common/dakota/fmuinterface.h \
    ../../common/parest.h \
    conf.h \
    labels.h \
    confapp.h \
    of_process.h \
    of_simulation.h

SOURCES += \
    ../../common/opt_project.cpp \
    ../../common/common.cpp \
    ../../common/formats/libcsv.c \
    ../../common/formats/read_csv.c \
    ../../common/formats/read_matlab4.c \
    ../../common/formats/write_matlab4.c \
    ../../common/opt_problem.cpp \
    ../../common/dakota/dakota_methods.cpp \
    ../../common/simulation.cpp \
    ../../common/parameterestimation.cpp \
    ../../common/dakota/fmuinterface.cpp \
    ../../common/parest.cpp \
    of_process.cpp \
    of_simulation.cpp \
    main.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#------------------------------------------------------------#
# Version is read from file "version"                        #
#------------------------------------------------------------#
FILE_VERSION  = "$$cat(version)"
SPLIT_VERSION = $$split(FILE_VERSION,".")

APP_VERSION_MAYOR = $$member(SPLIT_VERSION,0)
APP_VERSION_MINOR = $$member(SPLIT_VERSION,1)
APP_VERSION_BUILD = $$member(SPLIT_VERSION,2)

DEFINES += VERSION_MAJOR=$${APP_VERSION_MAYOR}
DEFINES += VERSION_MINOR=$${APP_VERSION_MINOR}
DEFINES += VERSION_BUILD=$${APP_VERSION_BUILD}

VERSION = $$sprintf("%1.%2.%3",$${APP_VERSION_MAYOR},$${APP_VERSION_MINOR},$${APP_VERSION_BUILD})

#------------------------------------------------------------#
# Libraries' paths                                           #
#------------------------------------------------------------#

COMMON_SRC = $${PWD}/../../common
FMIPP_SRC  = $${PWD}/../../../ThirdParty/src/fmipp-code
unix:      QUAZIP_SRC = $${PWD}/../../../ThirdParty/src/quazip-0.7.3/quazip
win32-g++: QUAZIP_SRC = $${PWD}/../../../ThirdParty/src/quazip-0.7.2/quazip
DAKOTA_SRC = $${PWD}/../../../ThirdParty/src/dakota-6.5.0.src
MPI_SRC    = "/usr/lib64/mpi/gcc/openmpi/include/"

FMIPP_LIB  = $${PWD}/../../../ThirdParty/build/fmipp-build
DAKOTA_LIB = $${PWD}/../../../ThirdParty/build/dakota-build
QUAZIP_LIB = $${PWD}/../../../ThirdParty/build/quazip-build

#------------------------------------------------------------#

INCLUDEPATH += $${COMMON_SRC} \                                                  # COMMON
               $${FMIPP_SRC}  \                                                  # FMI++
               $${QUAZIP_SRC} \                                                  # Quazip
               $${MPI_SRC}  \                                                    # MPI
               $${DAKOTA_SRC}/src/ \                                             # Dakota
               $${DAKOTA_SRC}/packages/external/teuchos/packages/teuchos/src/  \ # Dakota
               $${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/  \ # Dakota
               $${DAKOTA_SRC}/packages/pecos/src/  \                             # Dakota

LIBS     += -L$${FMIPP_LIB}/import/ -lfmippim \                                           # FMI++
            -L$${FMIPP_LIB}/export/ -lfmippex \                                           # FMI++
            -L$${QUAZIP_LIB} -lquazip \                                                   # Quazip
            -L$${DAKOTA_LIB}/src/ -ldakota_src \                                          # Dakota
            -L$${DAKOTA_LIB}/src/ -ldakota_dll_api \                                      # Dakota
            -L$${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ -lteuchos \  # Dakota
            #-L$${DAKOTA_LIB}/packages/pecos/src/ -lpecos_src \                            # Dakota
            -L$${DAKOTA_LIB}/packages/pecos/ -lpecos \                                    # Dakota
            -L$${DAKOTA_LIB}/packages/external/JEGA/FrontEnd/Core/ -ljega_fe \            # Dakota
            -L$${DAKOTA_LIB}/packages/external/NOMAD/src/ -lnomad \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/OPTPP/lib/ -loptpp \                       # Dakota
            -L$${DAKOTA_LIB}/packages/external/hopspack/src/ -lhopspack \                 # Dakota

# Profilling
# ----------
#LIBS += -lprofiler

# OpenMP support
# --------------
#QMAKE_CXXFLAGS += -fopenmp
#LIBS           += -fopenmp

# MPI Settings
# ------------
QMAKE_CXX         = mpicxx
QMAKE_LINK        = $$QMAKE_CXX
QMAKE_CC          = mpicc

QMAKE_CFLAGS           += $$system(mpicc --showme:compile)
QMAKE_LFLAGS           += $$system(mpicxx --showme:link)
QMAKE_CXXFLAGS         += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
QMAKE_CXXFLAGS_RELEASE += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
LIBS                   += -lmpi

# Path for libc in cluster
# ------------------------
# Since it is required GLIBC >= 2.14
GLIBC_PATH = /home/u5526/OptFMI/ThirdParty/build/glibc-build/

# Binary for Linux
# ----------------
QMAKE_RPATHDIR += $${FMIPP_LIB}/import/ \
                  $${FMIPP_LIB}/export/ \
                  $${QUAZIP_LIB} \
                  $${DAKOTA_LIB}/src/ \
                  $${DAKOTA_LIB}/packages/external/teuchos/packages/teuchos/src/ \
                  $${DAKOTA_LIB}/packages/pecos/ \
                  #$${DAKOTA_LIB}/packages/pecos/src/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/FrontEnd/Core/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/Utilities/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/eddy/utilities/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/eddy/threads/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/src/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/MOGA/ \
                  $${DAKOTA_LIB}/packages/external/JEGA/SOGA/ \
                  $${DAKOTA_LIB}/packages/external/NOMAD/src/ \
                  $${DAKOTA_LIB}/packages/external/OPTPP/lib/ \
                  $${DAKOTA_LIB}/packages/external/hopspack/src/ \
                  $${GLIBC_PATH}
