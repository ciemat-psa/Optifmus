# Opti<span style="color:blue">f</span>mus - Optimization of Functional Mock-up units (FMUs)
<img src="https://img.shields.io/badge/license-GPL%20v3-blue.svg">
Open source multi-platform software tool for optimization studies in Functional Mock-up Interface ([FMI](https://www.fmi-standard.org/)) compliant models.
<table cellspacing=0 cellpadding=0 border=0>
<tr><td align="center"> <img src="doc/snaps/main.png" height=300> </td>
       <td align="center"> <img src="doc/snaps/results.png" height=300> </td>
</tr>
<tr><td align="center"> <img src="doc/snaps/3dplot.png" height=300> </td>
       <td align="center"> <img src="doc/snaps/2dplot.png" height=300> </td>
</tr>
</table>

## Binaries

There are binaries for Linux and Windows platforms. 

* **Linux** - The binary for Linux is distributed as an x86_64 [AppImage](http://appimage.org/). The AppImage format is a format for packaging applications in a way that allows them to run on a variety of different target systems (base operating systems, distributions) without further modification. Just download the AppImage and mark it as executable. It has been tested in the following distributions, although it should work on most modern Linux distributions. The Optifmus AppImage requires Glibc 2.22 or above.

 * [openSUSE Leap 42.2 x86_64](https://en.opensuse.org/Portal:42.2).
 * [Ubuntu 16.04.2 LTS (Xenial Xerus) x86_64](http://releases.ubuntu.com/16.04/).
 * [Ubuntu 16.10 (Yakkety Yak) x86_64](http://releases.ubuntu.com/16.10/).
* **Windows** - An installer is provided for Windows. The app is distributed in 32-bit format. It has been tested in Windows 10 64 bits, although it should work on most Windows versions.

Linux binaries  | Windows installers
------------ | -------------
[Optifmus-0.1.0-x86_64.AppImage](https://gitlab.com/ciemat-psa/Optifmus/tree/master/bin/linux/Optifmus-0.1.0-x86_64.AppImage) | [Optifmus-0.1.0-installer.exe](https://gitlab.com/ciemat-psa/Optifmus/tree/master/bin/windows/Optifmus-0.1.0-installer.exe)
[ofc-0.1.0-x86_64.AppImage](https://gitlab.com/ciemat-psa/Optifmus/tree/master/bin/linux/ofc-0.1.0-x86_64.AppImage) |

## Examples

Download example projects directly from the [repository](https://gitlab.com/ciemat-psa/Optifmus/tree/master/examples). Click on the arrow at the top-right corner to download them as a compressed bundle file. Project files have the *qpf* extensions. Each project has a reference to a *FMU* file and can have a reference to an input file. The path of both files is relative to the project path. Therefore, the examples can be placed in any directory as long as the *FMU* and input files are also placed in the same relative paths.

## Development stage
<img src="https://img.shields.io/badge/stage-alpha-orange.svg">
Optifmus is currently at an early development stage, alpha stage. Therefore, there are missing features and may be bugs present.

## Optifmus tools

Two software tools are currently available.

* **Optifmus GUI** - Desktop tool with a Graphical User Interface (GUI). Optimization studies are configured through the GUI and can be loaded/stored in project files. The optimization process can be directly performed in Optifmus. Results are also shown in the GUI by means of tables and plots. They can be stored in the project file 
* **ofc** - Optifmus command line tool. It supports parallelization by means of Message Passing Interface (MPI). It receives as an argument a project file. Results are automatically stored in the project file.

## Optifmus software architecture

Optifmus relies on the following main libraries/toolkits.

* **[Qt 5 toolkit](https://www.qt.io/)** - Cross-platform application framework used for developing application software.
* **[FMI++](https://sourceforge.net/projects/fmipp)** - High-level utility package for FMI-based software development.
* **[Dakota](https://dakota.sandia.gov/)** - A powerful toolkit intended as a flexible, extensible interface between simulation codes and a variety of iterative systems analysis method.
* **[QuaZip](http://quazip.sourceforge.net/)** - A C++ wrapper for accessing zip format files.

## Building

Instructions are given as follows for compiling Optifmus in Linux and Windows platforms. Optifmus requires the following libraries/toolkits, main dependencies of those are also specified. Check third party documentation for knowing other dependencies; also check if those libraries/toolkits are available as packages or binaries in your operating system distribution since this may easy the compilation process. [Git](https://git-scm.com/) is necessary to download the Optifmus repository.

* [Qt 5 toolkit](https://www.qt.io/) - [Dependencies](https://wiki.qt.io/Qt5_dependencies)
* [Dakota](https://dakota.sandia.gov/) - [Dependencies](https://software.sandia.gov/trac/dakota/wiki/PlatformSpecificSetup)
 * [Blas](http://www.netlib.org/blas/)
 * [Lapack](http://www.netlib.org/lapack/)
 * [Boost](http://www.boost.org/)
 * [OpenMPI](https://www.open-mpi.org/) (optional)
* [FMI++](https://sourceforge.net/projects/fmipp)  - [Dependencies](https://sourceforge.net/projects/fmipp/files/fmipp_doc_v2015-02-11-17-29.pdf/download)
 * [Boost](http://www.boost.org/)
 * [Sundials](http://computation.llnl.gov/projects/sundials)
* [QuaZip](http://quazip.sourceforge.net/) - [Dependencies](http://quazip.sourceforge.net/)
 * [zlib](http://www.zlib.net/)

### Linux 

The [GNU Complier Collection (GCC)](https://gcc.gnu.org/) and [CMake](https://cmake.org/) are required in Linux.

* Check that dependencies and Qt 5 libraries are properly installed.

 ```bash
blas, blas-devel, lapack, lapack-devel, zlib, openmpi, openmpi-libs, openmpi-devel, sundials, sundials-devel, boost*, libboost*, etc.
 ```
 ```bash
libQt5Core5, libQt5Core-devel, libQt5Gui, libQt5Gui-devel, libQt5Charts5, libQt5Charts5-devel, libQt5DataVisualization5, libQt5DataVisualization5-devel, libQt5Widgets5, libQt5Widgets-devel, libQt5DBus5, libQt5DBus-devel, libQt5PrintSupport5, libQt5PrintSupport-devel, libQt5Svg5, libQt5DBus5, libQt5DBus-devel.
 ```

* If Boost is not available, compile it.

 ```bash
 cd <source_boost>
 ./bootstrap.sh --prefix=<build_boost>
 ./b2 -j4 install
 ```

* If Sundials is not available, compile it.

 ```bash
 cd <build_sundials>
 cmake <source_sundials>
 make -j4
 ```
* Compile QuaZip.

 ```bash
 cd <build_quazip>
 qmake <source_quazip>/quazip.pro -spec linux-g++ "CONFIG+=release"
 make -j4
 ```

* Compile FMI++. **NOTE:** Boost and Sundials may be automatically detected in that case the last three *CMake* arguments are not necessary.

 ```bash
cd <build_fmipp>
cmake <source_fmipp> \
      -DBOOST_ROOT=<build_boost> \
      -DSUNDIALS_INCLUDEDIR=<source_sundials>/include/ \
      -DSUNDIALS_LIBRARYDIR=<build_sundials>/src/cvode/ ; <build_sundials>/src/nvec_ser/
make -j4
 ```

* Compile Dakota. **NOTE:** Boost may be automatically detected in that case the last *CMake* argument is not necessary.

 ```bash
cd <build_dakota>
cmake -DCMAKE_INSTALL_PREFIX=<build_dakota> <source_dakota> \
      -DBOOST_ROOT=<build_boost>
make -j 4 
 ```
* Download Optifmus Git repository

 ```bash
cd <source_optifmus>
git clone https://gitlab.com/ciemat-psa/Optifmus.git
 ```

* Compile Optifmus. **NOTE:** specify all the path for headers (**INCLUDEPATH**) and libraries (**LIBS**) that are not automatically recognized by your operating system, see *[qmake](http://doc.qt.io/qt-5/qmake-manual.html)* arguments, second to last lines.

 ```bash
cd <build_Optifmus>
qmake <source_optifmus>/GUI/src/Optifmus.pro -spec linux-g++ "CONFIG+=release" \
      -INCLUDEPATH=<source_quazip> \
      -INCLUDEPATH=<source_boost> \
      -INCLUDEPATH=<source_sundials> \
      -INCLUDEPATH=<source_fmipp> \
      -INCLUDEPATH=<source_dakota> \
      -LIBS=-L<build_quazip> \
      -LIBS=-L<build_boost> \
      -LIBS=-L<build_sundials> \
      -LIBS=-L<build_fmipp> \
      -LIBS=-L<build_dakota>
make -j 4 
 ```

* Compile ofc. **NOTE**: same as for Optifmus compilation with respect to headers and libraries apply to ofc compilation.

 ```bash
cd <build_ofc>
qmake <source_optifmus>/ofc/src/ofc.pro -spec linux-g++ "CONFIG+=release"
      -INCLUDEPATH=<source_quazip> \
      -INCLUDEPATH=<source_boost> \
      -INCLUDEPATH=<source_sundials> \
      -INCLUDEPATH=<source_fmipp> \
      -INCLUDEPATH=<source_dakota> \
      -LIBS=-L<build_quazip> \
      -LIBS=-L<build_boost> \
      -LIBS=-L<build_sundials> \
      -LIBS=-L<build_fmipp> \
      -LIBS=-L<build_dakota>
make -j 4 
 ```

### Windows

Compilation in Windows is currently experimental. Dakota developers recommend compiling it in Microsoft Visual Studio. However, our current approach is to used [MSYS2](http://www.msys2.org/) and the MinGW compiler, 32-bit versions. Therefore, MSYS2 must be installed. MSYS2 uses [Pacman](https://www.archlinux.org/pacman/) as package manager. It must be used to install dependencies and libraries. Check required dependencies and libraries in the Linux compilation section. **NOTE:** MPI is not supported on Windows platforms.

* In MSYS2 shell, use Pacman to install dependencies and libraries, also Boost c++ libraries can be installed.

* Compile QuaZip.

 ```bash
 cd <build_quazip>
 qmake <source_quazip>/quazip.pro -spec linux-g++ LIBS+=-lz -spec win32-g++ "CONFIG+=release"
 mingw32-make
 ```

* Compile Sundials.

 ```bash
cd <build_sundials>
cmake -G "MinGW Makefiles" <source_sundials>
mingw32-make
 ```

* Compile FMI++. **NOTE:** Boost is automatically detected by MinGW in MSYS2.

 ```bash
cd <build_fmipp>
cmake <source_fmipp> -G "MinGW Makefiles" \
      -DSUNDIALS_INCLUDEDIR=<source_sundials>/include/ \
      -DSUNDIALS_LIBRARYDIR=<build_sundials>/src/cvode/ ; <build_sundials>/src/nvec_ser/
mingw32-make
 ```

* Compile Dakota. **NOTE:** In order to compile Dakota in the MSYS2 environment with the MinGW compiler, it is required to change some Dakota source code, mainly preprocessor directives in header files for taking into account the MinGW compiler in the compilation process. Required changes are documented [here](doc/Dakota_MSYS2_MinGW_changes.md).

 ```bash
cd <build_dakota>
cmake -DBUILD_SHARED_LIBS:BOOL=OFF \
      -DBUILD_STATIC_LIBS:BOOL=ON \
      -DHAVE_ACRO:BOOL=OFF \
      -G "MinGW Makefiles" \
      <source_dakota>
mingw32-make
 ```

* Download Optifmus Git repository

 ```bash
cd <source_optifmus>
git clone https://gitlab.com/ciemat-psa/Optifmus.git
 ```

* Compile Optifmus. **NOTE:** specify all the path for headers (**INCLUDEPATH**) and libraries (**LIBS**) that are not automatically recognized by your operating system, see *qmake* arguments, second to last lines.

 ```bash
cd <build_optifmus>
qmake <source_optifmus>/GUI/src/Optifmus.pro -spec win32-g++ "CONFIG+=release"
      -INCLUDEPATH=<source_quazip> \
      -INCLUDEPATH=<source_boost> \
      -INCLUDEPATH=<source_sundials> \
      -INCLUDEPATH=<source_fmipp> \
      -INCLUDEPATH=<source_dakota> \
      -LIBS=-L<build_quazip> \
      -LIBS=-L<build_boost> \
      -LIBS=-L<build_sundials> \
      -LIBS=-L<build_fmipp> \
      -LIBS=-L<build_dakota>
mingw32-make
 ```

* Compile ofc. **NOTE**: same as for Optifmus compilation with respect to headers and libraries apply to ofc compilation.

 ```bash
cd <build_ofc>
qmake <source_optifmus>/ofc/src/ofc.pro -spec win32-g++ "CONFIG+=release"
      -INCLUDEPATH=<source_quazip> \
      -INCLUDEPATH=<source_boost> \
      -INCLUDEPATH=<source_sundials> \
      -INCLUDEPATH=<source_fmipp> \
      -INCLUDEPATH=<source_dakota> \
      -LIBS=-L<build_quazip> \
      -LIBS=-L<build_boost> \
      -LIBS=-L<build_sundials> \
      -LIBS=-L<build_fmipp> \
      -LIBS=-L<build_dakota>
mingw32-make
 ```

## Publications

- ["Development of an open source multi-platform software tool for parameter estimation studies in FMI models"](https://www.modelica.org/events/modelica2017), J. Bonilla, J. Carballo, L. Roca & M. Berenguel, 12^th International Modelica Conference, 2017.

## Known Issues

The following known issues will be tackled in future software versions.

- [ ] Optimization processes take into account previous results. They cannot be restarted without closing the application.
- [ ] Refreshing *FMU* files does not currently work.
- [ ] FMI simulation log info is missing in the GUI. 
- [ ] Dymola exported *FMUs* which require a license to run failed to simulate when they are executed many times. This issue seems to be related to the access to the license. It only seems to happen on Windows platforms.
- [ ] The 3D plot does not work when executing the tool in VirtualBox. This seems to be related to VirtualBox driver which only support OpenGL v1.1, whereas the 3D plot requires v1.2 or higher.
- [ ] Recent project file list does not work on Windows platforms.
- [ ] When selected a particular point in a trajectory in the plotting tool, the value given in the hint box is not exact, it is only approximated.

## To-do list

The following features are currently missing in the tool.

* General
 - [ ] Most checks about the correctness of the info in the project and external files (*FMU* and input files) are missing.
 - [ ] When refreshing a *FMU* file info stored in the project (inputs, outputs, parameters, etc.) should be checked and deleted if missing.
* Model
 - [ ] Unit support in parameters.
 - [ ] Filters for model structure.
* Parameter estimation
 - [ ] Check if constraints are repeated.
 - [ ] Allow the user to simulate the model with selected estimated parameters.
 - [ ] More configuration options for 2D and 3D plots.
 - [ ] Include missing MOGA configuration options.
 - [ ] Include more optimization algorithms.
* Plotting tool
 * Import/export data
  - [ ] Open file/data.
  - [ ] Save (data, tab, graph, all).
  - [ ] Save formats (mat, CSV, etc.).
  - [ ] Save/copy clipboard (data, image).
  - [ ] Image (format, per graph/tab/all).
 * Visualization
  - [ ] Limit graphs depending on the screen size.
  - [ ] Configure axis.
  - [ ] Configure plot area.
  - [ ] Configure background.
  - [ ] Markers.
  - [ ] Special characters: latex support.
* Features
  - [ ] Independent variable.
  - [ ] Command line open file.
  - [ ] Unit management.
  - [ ] Filters for variables.

## ChangeLog

#### version 0.1.0
* First software version.

## Contributions

Any contributions to the Optifmus project are welcome, especially those addressing **known issues** and features in the **to-do** list. If you want to collaborate or have any idea, suggestion or recommendation, please contact the authors.

## License & disclaimer

Optifmus is distributed under the GNU General Public License version 3. Check terms, conditions and disclaimer in [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
***
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
